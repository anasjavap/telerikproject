package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Department;
import com.telerik.datamodel.UserInfo;

public interface UserRepository extends PagingAndSortingRepository<UserInfo, Long> {

	public UserInfo findByUsername(String username);
	public List<UserInfo> findByDepartment(Department department);
}
