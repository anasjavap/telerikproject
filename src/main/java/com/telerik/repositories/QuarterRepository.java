package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Quarter;

public interface QuarterRepository extends PagingAndSortingRepository<Quarter, Long> {

}
