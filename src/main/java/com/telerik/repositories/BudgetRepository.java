package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Budget;
import com.telerik.datamodel.Faaliyet;

public interface BudgetRepository extends PagingAndSortingRepository<Budget, Long> {

	List<Budget> findByFaaliyet(Faaliyet faaliyet);
}
