package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.GostergeOlcumSekil;

public interface GostergeOlcumSekilRepository extends PagingAndSortingRepository<GostergeOlcumSekil, Long> {

}
