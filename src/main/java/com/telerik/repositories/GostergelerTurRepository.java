package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.GostergelerTur;

public interface GostergelerTurRepository extends PagingAndSortingRepository<GostergelerTur, Long> {

}
