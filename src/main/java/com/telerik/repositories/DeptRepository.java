package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Department;

public interface DeptRepository extends PagingAndSortingRepository<Department, Long> {

}
