package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Performans;
import com.telerik.datamodel.StrategyPlanHedefler;

public interface PerformansRepository extends PagingAndSortingRepository<Performans, Long> {

	List<Performans> findByHedefler(StrategyPlanHedefler hedefler);
}
