package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Department;
import com.telerik.datamodel.StrategyPlanAmaclar;

public interface StrategyPlanAmaclarRepository extends PagingAndSortingRepository<StrategyPlanAmaclar, Long>{

	List<StrategyPlanAmaclar> findByDepartment(Department department);
	
	List<StrategyPlanAmaclar> findByDepartment_deptId(Long id);
}
