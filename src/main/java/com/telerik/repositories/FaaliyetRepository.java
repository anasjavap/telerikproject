package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Faaliyet;
import com.telerik.datamodel.PerformansHedefi;

public interface FaaliyetRepository extends PagingAndSortingRepository<Faaliyet,Long> {

	public List<Faaliyet> findByPerformansHedefi(PerformansHedefi performansHedefi);
	public List<Faaliyet> findByPerformansHedefi_Hedef_amaclar_department_deptId(Long id);
}
