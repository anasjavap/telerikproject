package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Maliyeti;

public interface MaliyetiRepository extends PagingAndSortingRepository<Maliyeti , Long> {

}
