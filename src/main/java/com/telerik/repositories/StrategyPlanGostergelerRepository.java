package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Department;
import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanGostergeler;

public interface StrategyPlanGostergelerRepository extends PagingAndSortingRepository<StrategyPlanGostergeler,Long> {

	List<StrategyPlanGostergeler> findByPerformansHedefi(PerformansHedefi performansHedefi);
	List<StrategyPlanGostergeler> findByPerformansHedefi_Hedef_Amaclar_Department_deptId(Long id);
	
}