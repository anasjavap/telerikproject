package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Gerceklesen;
import com.telerik.datamodel.StrategyPlanGostergeler;

public interface GerceklesenRepository extends PagingAndSortingRepository<Gerceklesen,Long> {

	List<Gerceklesen> findByGostergeler(StrategyPlanGostergeler gostergeler);
}
