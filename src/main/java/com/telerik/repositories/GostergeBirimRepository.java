package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.GostergeBirim;

public interface GostergeBirimRepository extends PagingAndSortingRepository<GostergeBirim, Long> {

}
