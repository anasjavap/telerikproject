package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.GostergeAdet;

public interface GostergeAdetRepository extends PagingAndSortingRepository<GostergeAdet, Long> {

}
