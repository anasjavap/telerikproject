package com.telerik.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.Role;

public interface RolesRepository extends PagingAndSortingRepository<Role, Long> {

}
