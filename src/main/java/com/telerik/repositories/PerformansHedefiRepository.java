package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanHedefler;

public interface PerformansHedefiRepository extends PagingAndSortingRepository<PerformansHedefi, Long> {

	public List<PerformansHedefi> findByHedef(StrategyPlanHedefler hedefler);
}
