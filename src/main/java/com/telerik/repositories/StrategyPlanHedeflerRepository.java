package com.telerik.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.telerik.datamodel.StrategyPlanAmaclar;
import com.telerik.datamodel.StrategyPlanHedefler;

public interface StrategyPlanHedeflerRepository extends PagingAndSortingRepository<StrategyPlanHedefler, Long> {
	
	List<StrategyPlanHedefler> findByAmaclar(StrategyPlanAmaclar amaclar);
}
