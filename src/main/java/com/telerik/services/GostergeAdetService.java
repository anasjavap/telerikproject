package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.GostergeAdet;

public interface GostergeAdetService {

	public List<GostergeAdet> getAllAdet();
	public GostergeAdet getById(Long id);
	public void updateAdet(GostergeAdet adet) throws Exception;
	public void deleteAdet(GostergeAdet adet) throws Exception;
	public void addAdet(GostergeAdet adet) throws Exception;
	
}
