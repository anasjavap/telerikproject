package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanHedefler;

public interface PerformansHedefiService {

	public List<PerformansHedefi> getAll();
	
	public PerformansHedefi getById(Long id);
	public List<PerformansHedefi> getByHedef(StrategyPlanHedefler hedef) ;
	public void updatePerformansHedef(PerformansHedefi performans) throws Exception;
	public void addPerformansHedef(PerformansHedefi performans) throws Exception;
	public void deletePerformansHedef(PerformansHedefi performans) throws Exception;
	
}
