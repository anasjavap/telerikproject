package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.StrategyPlanHedefler;

public interface StrategyPlanHedeflerService {

	public StrategyPlanHedefler getHedefById(Long id);
	
	public List<StrategyPlanHedefler> getHedeflerByAmacId(Long id);
	public void addHedef(StrategyPlanHedefler planHedefler);
	public void deleteHedef(StrategyPlanHedefler planHedefler) throws Exception;
	public void updateHedef(StrategyPlanHedefler planHedefler) throws Exception;
	
}
