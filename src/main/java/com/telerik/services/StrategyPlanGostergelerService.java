package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanGostergeler;

public interface StrategyPlanGostergelerService {

	public StrategyPlanGostergeler getById(Long id);
	public List<StrategyPlanGostergeler> getByDepartment(Long id);
	public List<StrategyPlanGostergeler> getAllGosterge();
	public List<StrategyPlanGostergeler> getByPerformans(PerformansHedefi performansHedefi);
	public void addGosterge(StrategyPlanGostergeler planGostergeler);
	public void deleteGosterge(StrategyPlanGostergeler planGostergeler) throws Exception;
	public void updateGosterge(StrategyPlanGostergeler planGostergeler) throws Exception;
}
