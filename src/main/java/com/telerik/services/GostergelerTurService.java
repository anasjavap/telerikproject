package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.GostergelerTur;

public interface GostergelerTurService {

	public List<GostergelerTur> getAllTur();
	public GostergelerTur getById(Long id);
	public void updateTue(GostergelerTur gostergelerTur) throws Exception;
	public void addTue(GostergelerTur gostergelerTur) throws Exception;
	public void deleteTue(GostergelerTur gostergelerTur) throws Exception;
	
}
