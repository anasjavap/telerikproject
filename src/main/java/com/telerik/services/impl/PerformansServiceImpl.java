package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.Performans;
import com.telerik.datamodel.StrategyPlanHedefler;
import com.telerik.repositories.PerformansRepository;
import com.telerik.services.PerformansService;

@Service("performansService")
public class PerformansServiceImpl implements PerformansService {

	@Autowired
	private PerformansRepository performansRepository;
	
	@Override
	public List<Performans> getAllPerformans() {
		List<Performans> performans =  (List<Performans>) performansRepository.findAll();
		return performans;
	}

	@Override
	public void updatePerformans(Performans performans) throws Exception{
		performansRepository.save(performans);
		
	}

	@Override
	public void addPerformans(Performans performans) throws Exception{
		performansRepository.save(performans);
		
	}

	@Override
	public void deletePerformans(Performans performans) throws Exception{
		performansRepository.delete(performans);
		
	}

	@Override
	public List<Performans> getByHedef(StrategyPlanHedefler hedefler) {
		List<Performans> performans = performansRepository.findByHedefler(hedefler);
		return performans;
	}

	public PerformansRepository getPerformansRepository() {
		return performansRepository;
	}

	public void setPerformansRepository(PerformansRepository performansRepository) {
		this.performansRepository = performansRepository;
	}

}
