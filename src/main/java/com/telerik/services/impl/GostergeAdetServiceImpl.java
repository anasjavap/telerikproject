package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.GostergeAdet;
import com.telerik.repositories.GostergeAdetRepository;
import com.telerik.services.GostergeAdetService;

@Service("gostergeAdetService")
public class GostergeAdetServiceImpl implements GostergeAdetService {

	@Autowired
	private GostergeAdetRepository adetRepository;
	
	
	@Override
	public List<GostergeAdet> getAllAdet() {
		List<GostergeAdet> adet = (List<GostergeAdet>) adetRepository.findAll();
		return adet;
	}


	@Override
	public void updateAdet(GostergeAdet adet) throws Exception{
		adetRepository.save(adet);
		
	}


	@Override
	public void deleteAdet(GostergeAdet adet) throws Exception{
		adetRepository.delete(adet);
		
	}


	@Override
	public void addAdet(GostergeAdet adet) throws Exception{
		adetRepository.save(adet);
		
	}


	@Override
	public GostergeAdet getById(Long id) {
		
		return adetRepository.findOne(id);
	}


	public GostergeAdetRepository getAdetRepository() {
		return adetRepository;
	}


	public void setAdetRepository(GostergeAdetRepository adetRepository) {
		this.adetRepository = adetRepository;
	}

}
