package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.Gerceklesen;
import com.telerik.datamodel.StrategyPlanGostergeler;
import com.telerik.repositories.GerceklesenRepository;
import com.telerik.services.GerceklesenService;

@Service("gerceklesenService")
public class GerceklesenServiceImpl implements GerceklesenService {

	@Autowired
	private GerceklesenRepository gerceklesenRepository;
	
	@Override
	public List<Gerceklesen> getAllGerceklesen() {
		List<Gerceklesen> gerceklesens =  (List<Gerceklesen>) gerceklesenRepository.findAll();
		return gerceklesens;
	}

	@Override
	public void updateGerceklesen(Gerceklesen gerceklesen) throws Exception {
		gerceklesenRepository.save(gerceklesen);
		
	}

	@Override
	public void deleteGerceklesen(Gerceklesen gerceklesen) throws Exception{
		gerceklesenRepository.delete(gerceklesen);
		
	}

	@Override
	public void addGerceklesen(Gerceklesen gerceklesen) throws Exception{
		gerceklesenRepository.save(gerceklesen);
		
	}

	@Override
	public List<Gerceklesen> getByGosterge(StrategyPlanGostergeler gostergeler) {
		return gerceklesenRepository.findByGostergeler(gostergeler);
	}

	public GerceklesenRepository getGerceklesenRepository() {
		return gerceklesenRepository;
	}

	public void setGerceklesenRepository(GerceklesenRepository gerceklesenRepository) {
		this.gerceklesenRepository = gerceklesenRepository;
	}

}
