package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.StrategyPlanHedefler;
import com.telerik.repositories.StrategyPlanAmaclarRepository;
import com.telerik.repositories.StrategyPlanHedeflerRepository;
import com.telerik.services.StrategyPlanHedeflerService;

@Service("StrategyPlanHedeflerService")
public class StrategyPlanHedeflerServiceImpl implements StrategyPlanHedeflerService {

	@Autowired
	private StrategyPlanHedeflerRepository hedeflerRepository;
	@Autowired
	private StrategyPlanAmaclarRepository amaclarRepository;

	@Override
	public StrategyPlanHedefler getHedefById(Long id) {
		return hedeflerRepository.findOne(id);
	}

	@Override
	public List<StrategyPlanHedefler> getHedeflerByAmacId(Long id) {
		return hedeflerRepository.findByAmaclar(amaclarRepository.findOne(id));
	}

	@Override
	public void addHedef(StrategyPlanHedefler planHedefler) {
		hedeflerRepository.save(planHedefler);
		
	}

	@Override
	public void deleteHedef(StrategyPlanHedefler planHedefler) throws Exception {
		hedeflerRepository.delete(planHedefler);
	}

	@Override
	public void updateHedef(StrategyPlanHedefler planHedefler) throws Exception {
		hedeflerRepository.save(planHedefler);
		
	}

	public StrategyPlanHedeflerRepository getHedeflerRepository() {
		return hedeflerRepository;
	}

	public void setHedeflerRepository(StrategyPlanHedeflerRepository hedeflerRepository) {
		this.hedeflerRepository = hedeflerRepository;
	}

	public StrategyPlanAmaclarRepository getAmaclarRepository() {
		return amaclarRepository;
	}

	public void setAmaclarRepository(StrategyPlanAmaclarRepository amaclarRepository) {
		this.amaclarRepository = amaclarRepository;
	}

}
