package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanHedefler;
import com.telerik.repositories.PerformansHedefiRepository;
import com.telerik.services.PerformansHedefiService;

@Service("performansHedefiService")
public class PerformansHedefiServiceImpl implements PerformansHedefiService {

	@Autowired
	private PerformansHedefiRepository performansHedefiRepository;
	
	@Override
	public List<PerformansHedefi> getAll() {
		List<PerformansHedefi> performansHedefis = (List<PerformansHedefi>) performansHedefiRepository.findAll();
		return performansHedefis;
	}

	@Override
	public PerformansHedefi getById(Long id) {
		return performansHedefiRepository.findOne(id);
	}

	@Override
	public void updatePerformansHedef(PerformansHedefi performans) throws Exception {
		performansHedefiRepository.save(performans);
	}

	@Override
	public void addPerformansHedef(PerformansHedefi performans) throws Exception {
		performansHedefiRepository.save(performans);
		
	}

	@Override
	public void deletePerformansHedef(PerformansHedefi performans) throws Exception {
		performansHedefiRepository.delete(performans);
		
	}

	@Override
	public List<PerformansHedefi> getByHedef(StrategyPlanHedefler hedef) {
		List<PerformansHedefi> hedefis = performansHedefiRepository.findByHedef(hedef);
		return hedefis;
	}

	public PerformansHedefiRepository getPerformansHedefiRepository() {
		return performansHedefiRepository;
	}

	public void setPerformansHedefiRepository(PerformansHedefiRepository performansHedefiRepository) {
		this.performansHedefiRepository = performansHedefiRepository;
	}

}
