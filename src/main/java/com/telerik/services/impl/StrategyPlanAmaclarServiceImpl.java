package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telerik.datamodel.Department;
import com.telerik.datamodel.StrategyPlanAmaclar;
import com.telerik.repositories.StrategyPlanAmaclarRepository;
import com.telerik.services.StrategyPlanAmaclarService;

@Service("StrategyPlanAmaclarService")
public class StrategyPlanAmaclarServiceImpl implements StrategyPlanAmaclarService {

	@Autowired
	private StrategyPlanAmaclarRepository amaclarRepository; 
	
	@Override
	@Transactional
	public List<StrategyPlanAmaclar> getAllAmaclar() {
		List<StrategyPlanAmaclar> amaclar = (List<StrategyPlanAmaclar>) amaclarRepository.findAll();
		return amaclar;
	}

	@Override
	@Transactional
	public StrategyPlanAmaclar getAmacById(Long id) {
		return amaclarRepository.findOne(id);
	}

	@Override
	@Transactional
	public void addAmac(StrategyPlanAmaclar planAmaclar) {
		amaclarRepository.save(planAmaclar);
		
	}

	@Override
	@Transactional
	public void deleteAmac(StrategyPlanAmaclar planAmaclar){
		amaclarRepository.delete(planAmaclar);	
	}

	@Override
	@Transactional
	public void updateAmac(StrategyPlanAmaclar planAmaclar) {
		amaclarRepository.save(planAmaclar);
		
	}

	@Override
	@Transactional
	public List<StrategyPlanAmaclar> getByDept(Long id) {
		List<StrategyPlanAmaclar> amaclar = amaclarRepository.findByDepartment_deptId(id);
		return amaclar;
	}

	public StrategyPlanAmaclarRepository getAmaclarRepository() {
		return amaclarRepository;
	}

	public void setAmaclarRepository(StrategyPlanAmaclarRepository amaclarRepository) {
		this.amaclarRepository = amaclarRepository;
	}

}
