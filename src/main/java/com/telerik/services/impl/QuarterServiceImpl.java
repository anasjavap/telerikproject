package com.telerik.services.impl;

import org.springframework.stereotype.Service;

import com.telerik.datamodel.Quarter;
import com.telerik.repositories.QuarterRepository;
import com.telerik.services.QuarterService;

@Service("quarterService")
public class QuarterServiceImpl implements QuarterService {

	
	private QuarterRepository quarterRepository;
	@Override
	public void addQuarter(Quarter quarter) {
		// TODO Auto-generated method stub
		quarterRepository.save(quarter);
		
	}

	@Override
	public void updateQuarter(Quarter quarter) {
		// TODO Auto-generated method stub
		quarterRepository.save(quarter);
		
	}

	@Override
	public void deleteQuarter(Quarter quarter) {
		// TODO Auto-generated method stub
		quarterRepository.delete(quarter);
		
	}

	public QuarterRepository getQuarterRepository() {
		return quarterRepository;
	}

	public void setQuarterRepository(QuarterRepository quarterRepository) {
		this.quarterRepository = quarterRepository;
	}

}
