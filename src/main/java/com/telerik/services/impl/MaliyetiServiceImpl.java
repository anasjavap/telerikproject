package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.Maliyeti;
import com.telerik.repositories.MaliyetiRepository;
import com.telerik.services.MaliyetiService;

@Service("maliyetiService")
public class MaliyetiServiceImpl implements MaliyetiService {

	@Autowired
	private MaliyetiRepository maliyetiRepository;
	
	@Override
	public List<Maliyeti> getAllMaliyeti() {
		List<Maliyeti> maliyetis = (List<Maliyeti>) maliyetiRepository.findAll();
		return maliyetis;
	}

	@Override
	public void updateMaliyeti(Maliyeti maliyeti) throws Exception{
		maliyetiRepository.save(maliyeti);
	}

	@Override
	public void addMaliyeti(Maliyeti maliyeti) throws Exception {
		maliyetiRepository.save(maliyeti);
		
	}

	@Override
	public void deleteMaliyeti(Maliyeti maliyeti) throws Exception {
		maliyetiRepository.delete(maliyeti);
		
	}

	public MaliyetiRepository getMaliyetiRepository() {
		return maliyetiRepository;
	}

	public void setMaliyetiRepository(MaliyetiRepository maliyetiRepository) {
		this.maliyetiRepository = maliyetiRepository;
	}

}
