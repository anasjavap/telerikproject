package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanGostergeler;
import com.telerik.repositories.StrategyPlanGostergelerRepository;
import com.telerik.services.StrategyPlanGostergelerService;

@Service("StrategyPlanGostergelerService")
public class StrategyPlanGostergelerServiceImpl implements StrategyPlanGostergelerService {

	@Autowired
	private StrategyPlanGostergelerRepository gostergelerRepository;
	
//	@Override
//	public List<StrategyPlanGostergeler> getGostergelerByHedefId(Long id) {
//		return gostergelerRepository.findByHedefler(hedeflerRepository.findOne(id));
//	}

	@Override
	public void addGosterge(StrategyPlanGostergeler planGostergeler) {
		gostergelerRepository.save(planGostergeler);
		
	}

	@Override
	public void deleteGosterge(StrategyPlanGostergeler planGostergeler) throws Exception {
		gostergelerRepository.delete(planGostergeler);
	}

	@Override
	public void updateGosterge(StrategyPlanGostergeler planGostergeler) throws Exception {
		gostergelerRepository.save(planGostergeler);
	}

	@Override
	public List<StrategyPlanGostergeler> getAllGosterge() {
		 List<StrategyPlanGostergeler> gostergelers = (List<StrategyPlanGostergeler>) gostergelerRepository.findAll();
		return gostergelers;
	}

	@Override
	public List<StrategyPlanGostergeler> getByPerformans(PerformansHedefi performansHedefi) {
		List<StrategyPlanGostergeler> gostergelers=gostergelerRepository.findByPerformansHedefi(performansHedefi);
		return gostergelers;
	}

	@Override
	public StrategyPlanGostergeler getById(Long id) {
		
		return gostergelerRepository.findOne(id);
	}

	public StrategyPlanGostergelerRepository getGostergelerRepository() {
		return gostergelerRepository;
	}

	public void setGostergelerRepository(StrategyPlanGostergelerRepository gostergelerRepository) {
		this.gostergelerRepository = gostergelerRepository;
	}

	@Override
	public List<StrategyPlanGostergeler> getByDepartment(Long id) {
		return gostergelerRepository.findByPerformansHedefi_Hedef_Amaclar_Department_deptId(id);
	}

}
