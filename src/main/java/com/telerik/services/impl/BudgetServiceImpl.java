package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.Budget;
import com.telerik.datamodel.Faaliyet;
import com.telerik.repositories.BudgetRepository;
import com.telerik.services.BudgetService;

@Service("budgetService")
public class BudgetServiceImpl  implements BudgetService{

	@Autowired
	private BudgetRepository budgetRepository;
	
	@Override
	public List<Budget> getByFaaliyet(Faaliyet faaliyet) {
		
		return budgetRepository.findByFaaliyet(faaliyet);
	}

	@Override
	public void addBudget(Budget budget) {
		budgetRepository.save(budget);
		
	}

	@Override
	public void updateBudget(Budget budget) {
		budgetRepository.save(budget);
		
	}

	@Override
	public void deleteBudget(Budget budget) {
		budgetRepository.delete(budget);
		
	}

	public BudgetRepository getBudgetRepository() {
		return budgetRepository;
	}

	public void setBudgetRepository(BudgetRepository budgetRepository) {
		this.budgetRepository = budgetRepository;
	}

	
}
