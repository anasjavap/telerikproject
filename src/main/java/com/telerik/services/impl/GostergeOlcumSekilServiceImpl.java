package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.GostergeOlcumSekil;
import com.telerik.repositories.GostergeOlcumSekilRepository;
import com.telerik.services.GostergeOlcumSekilService;

@Service("gostergeOlcumSekilService")
public class GostergeOlcumSekilServiceImpl implements GostergeOlcumSekilService {

	@Autowired
	private GostergeOlcumSekilRepository olcumSekilRepository;
	
	@Override
	public List<GostergeOlcumSekil> getAllOlcum() {
		List<GostergeOlcumSekil> olcum=  (List<GostergeOlcumSekil>) olcumSekilRepository.findAll();
		return olcum;
	}

	@Override
	public void updateOclum(GostergeOlcumSekil gostergeOlcumSekil) throws Exception {
		olcumSekilRepository.save(gostergeOlcumSekil);
		
	}

	@Override
	public void deleteOclum(GostergeOlcumSekil gostergeOlcumSekil) throws Exception {
		olcumSekilRepository.delete(gostergeOlcumSekil);
		
	}

	@Override
	public void addOclum(GostergeOlcumSekil gostergeOlcumSekil) throws Exception {
		olcumSekilRepository.save(gostergeOlcumSekil);
		
	}

	@Override
	public GostergeOlcumSekil getById(Long id) {
		return olcumSekilRepository.findOne(id);
	}

	public GostergeOlcumSekilRepository getOlcumSekilRepository() {
		return olcumSekilRepository;
	}

	public void setOlcumSekilRepository(GostergeOlcumSekilRepository olcumSekilRepository) {
		this.olcumSekilRepository = olcumSekilRepository;
	}


}
