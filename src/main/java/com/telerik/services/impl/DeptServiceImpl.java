package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telerik.datamodel.Department;
import com.telerik.repositories.DeptRepository;
import com.telerik.services.DeptService;

@Service("deptService")
public class DeptServiceImpl implements DeptService {

	@Autowired
	private DeptRepository deptRepository;
	
	@Override
	@Transactional
	public List<Department> getAllDept() {
		List<Department> depts = (List<Department>) deptRepository.findAll();
		return depts;
	}

	@Override
	public void addDept(Department dept) {
		deptRepository.save(dept);
	}

	@Override
	public void updateDept(Department dept) {
		deptRepository.save(dept);
	}

	@Override
	public boolean deleteDept(Department dept){
		try{
			deptRepository.delete(dept);
			return true;
		}catch(Exception e){
			return false;
		}	
	}

	@Override
	public Department getDeptbyId(Long id) {
		return deptRepository.findOne(id);
	}

	public DeptRepository getDeptRepository() {
		return deptRepository;
	}

	public void setDeptRepository(DeptRepository deptRepository) {
		this.deptRepository = deptRepository;
	}

}
