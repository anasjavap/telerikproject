package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.Faaliyet;
import com.telerik.datamodel.PerformansHedefi;
import com.telerik.repositories.FaaliyetRepository;
import com.telerik.services.FaaliyetService;


@Service("faaliyetService") 
public class FaaliyetServiceImpl implements FaaliyetService {

	
	@Autowired
	private FaaliyetRepository faaliyetRepository;
	
	
	@Override
	public List<Faaliyet> getAllFaaliyet() {		
		List<Faaliyet> faaliyets =  (List<Faaliyet>) faaliyetRepository.findAll();
		return faaliyets;
	}


	@Override
	public void updateFaaliyet(Faaliyet faaliyet) throws Exception{
		faaliyetRepository.save(faaliyet);
		
	}


	@Override
	public void deleteFaaliyet(Faaliyet faaliyet) throws Exception{
		faaliyetRepository.delete(faaliyet);
		
	}


	@Override
	public void addFaaliyet(Faaliyet faaliyet) throws Exception{
		faaliyetRepository.save(faaliyet);
		
	}


	@Override
	public List<Faaliyet> getByPerformansHedefi(PerformansHedefi performansHedefi) {
		return faaliyetRepository.findByPerformansHedefi(performansHedefi);
	}


	@Override
	public Faaliyet getById(Long id) {
		
		return faaliyetRepository.findOne(id);
	}


	public FaaliyetRepository getFaaliyetRepository() {
		return faaliyetRepository;
	}


	public void setFaaliyetRepository(FaaliyetRepository faaliyetRepository) {
		this.faaliyetRepository = faaliyetRepository;
	}


	@Override
	public List<Faaliyet> getByDepartment(Long id) {
		return faaliyetRepository.findByPerformansHedefi_Hedef_amaclar_department_deptId(id);
	}

}
