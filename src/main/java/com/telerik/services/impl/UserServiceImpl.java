package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.telerik.datamodel.Department;
import com.telerik.datamodel.UserInfo;
import com.telerik.repositories.UserRepository;
import com.telerik.services.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void addUser(UserInfo user) {
		userRepository.save(user);
	}

	@Override
	public void editUser(UserInfo user) {
		userRepository.save(user);

	}

	@Override
	public boolean deleteUser(Long id) {
		try {
			UserInfo us  = userRepository.findOne(id);
			userRepository.delete(us);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean disActiveUser(Long id) {
		try {
			UserInfo us=userRepository.findOne(id);
			us.setActive(false);
			userRepository.save(us);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean activeUser(Long id) {
		try {
			UserInfo us=userRepository.findOne(id);
			us.setActive(true);
			userRepository.save(us);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserInfo getUser(String username) {
		return	userRepository.findByUsername(username);
	}

	@Override
	public List<UserInfo> getAllUser() {
		return (List<UserInfo>) userRepository.findAll();
	}

	@Override
	public List<UserInfo> getUsersByDept(Department department) {
		List<UserInfo> users = userRepository.findByDepartment(department);
		return users;
	}

	
	
}
