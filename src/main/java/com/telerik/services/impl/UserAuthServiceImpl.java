package com.telerik.services.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.UserInfo;
import com.telerik.services.UserAuthService;
import com.telerik.services.UserService;

@Service("UserAuthService")
public class UserAuthServiceImpl implements UserAuthService, UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserInfo user = userService.getUser(username);
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		SimpleGrantedAuthority userAuthority = new SimpleGrantedAuthority("ROLE_USER");
		SimpleGrantedAuthority adminAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");

		if (user.getRole().getName().equals("user")) {
			authorities.add(userAuthority);
		} else if (user.getRole().getName().equals("admin")) {
			//authorities.add(userAuthority);
			authorities.add(adminAuthority);
		}
		UserDetails details =
		new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),user.isActive(),true,true,true,authorities);
		return details;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	

}
