package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.GostergelerTur;
import com.telerik.repositories.GostergelerTurRepository;
import com.telerik.services.GostergelerTurService;

@Service("gostergelerTurService")
public class GostergelerTurServiceImpl implements GostergelerTurService {
	
	@Autowired
	private GostergelerTurRepository turRepository;  

	@Override
	public List<GostergelerTur> getAllTur() {
		List<GostergelerTur> turs =  (List<GostergelerTur>) turRepository.findAll();
		return turs;
	}

	@Override
	public void updateTue(GostergelerTur gostergelerTur) throws Exception{
		turRepository.save(gostergelerTur);
		
	}

	@Override
	public void addTue(GostergelerTur gostergelerTur) throws Exception {
		turRepository.save(gostergelerTur);
		
	}

	@Override
	public void deleteTue(GostergelerTur gostergelerTur) throws Exception{
		turRepository.delete(gostergelerTur);
		
	}

	@Override
	public GostergelerTur getById(Long id) {
		return turRepository.findOne(id);
	}

	public GostergelerTurRepository getTurRepository() {
		return turRepository;
	}

	public void setTurRepository(GostergelerTurRepository turRepository) {
		this.turRepository = turRepository;
	}

}
