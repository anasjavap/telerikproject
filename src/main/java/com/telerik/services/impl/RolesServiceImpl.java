package com.telerik.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telerik.datamodel.Role;
import com.telerik.repositories.RolesRepository;
import com.telerik.services.RolesService;

@Service("rolesService")
public class RolesServiceImpl implements RolesService {

	@Autowired
	private RolesRepository rolesRepository;
	
	@Override
	public List<Role> getAllRoles() {
		return (List<Role>) rolesRepository.findAll();
	}

	@Override
	public Role getRoleById(Long id) {
		
		return rolesRepository.findOne(id);
	}

	public RolesRepository getRolesRepository() {
		return rolesRepository;
	}

	public void setRolesRepository(RolesRepository rolesRepository) {
		this.rolesRepository = rolesRepository;
	}

}
