package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Maliyeti;

public interface MaliyetiService {
	
	public List<Maliyeti> getAllMaliyeti();
	public void updateMaliyeti(Maliyeti maliyeti)throws Exception;
	public void addMaliyeti(Maliyeti maliyeti) throws Exception;
	public void deleteMaliyeti(Maliyeti maliyeti) throws Exception;

}
