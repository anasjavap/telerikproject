package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Department;

public interface DeptService {
	
	public List<Department> getAllDept();
	public Department getDeptbyId(Long id);
	public void addDept(Department dept);
	public void updateDept(Department dept);
	public boolean deleteDept(Department dept);

}
