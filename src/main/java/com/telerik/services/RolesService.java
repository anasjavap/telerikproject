package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Role;

public interface RolesService {
	
	public List<Role> getAllRoles();
	public Role getRoleById(Long id);

}
