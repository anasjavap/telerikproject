package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Department;
import com.telerik.datamodel.StrategyPlanAmaclar;

public interface StrategyPlanAmaclarService {

	public List<StrategyPlanAmaclar> getAllAmaclar();
	public StrategyPlanAmaclar getAmacById(Long id);
	public void addAmac(StrategyPlanAmaclar planAmaclar);
	public void deleteAmac(StrategyPlanAmaclar planAmaclar);
	public void updateAmac(StrategyPlanAmaclar planAmaclar);
	public List<StrategyPlanAmaclar> getByDept(Long id);
}
