package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.GostergeOlcumSekil;

public interface GostergeOlcumSekilService {

	public List<GostergeOlcumSekil> getAllOlcum();
	public GostergeOlcumSekil getById(Long id);
	public void updateOclum(GostergeOlcumSekil gostergeOlcumSekil) throws Exception;
	public void deleteOclum(GostergeOlcumSekil gostergeOlcumSekil) throws Exception;
	public void addOclum(GostergeOlcumSekil gostergeOlcumSekil) throws Exception;

	
}
