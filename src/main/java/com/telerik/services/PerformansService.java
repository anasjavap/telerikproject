package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Performans;
import com.telerik.datamodel.StrategyPlanHedefler;

public interface PerformansService {

	public List<Performans> getAllPerformans();
	public List<Performans> getByHedef(StrategyPlanHedefler hedefler);
	public void updatePerformans(Performans performans) throws Exception;
	public void addPerformans(Performans performans) throws Exception;
	public void deletePerformans(Performans performans) throws Exception;
	
}
