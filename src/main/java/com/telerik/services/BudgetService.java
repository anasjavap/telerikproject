package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Budget;
import com.telerik.datamodel.Faaliyet;

public interface BudgetService {

	public List<Budget> getByFaaliyet(Faaliyet faaliyet);
	public void addBudget(Budget budget);
	public void updateBudget(Budget budget);
	public void deleteBudget(Budget budget);
	
}
