package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Gerceklesen;
import com.telerik.datamodel.StrategyPlanGostergeler;

public interface GerceklesenService {

	
	public List<Gerceklesen> getAllGerceklesen();
	public List<Gerceklesen> getByGosterge(StrategyPlanGostergeler gostergeler);
	public void updateGerceklesen(Gerceklesen gerceklesen) throws Exception;
	public void deleteGerceklesen(Gerceklesen gerceklesen) throws Exception;
	public void addGerceklesen(Gerceklesen gerceklesen) throws Exception;
	
}
