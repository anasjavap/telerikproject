package com.telerik.services;

import com.telerik.datamodel.Quarter;

public interface QuarterService {

	public void addQuarter(Quarter quarter);
	public void updateQuarter(Quarter quarter);
	public void deleteQuarter(Quarter quarter);
}
