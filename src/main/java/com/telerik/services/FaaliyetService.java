package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Faaliyet;
import com.telerik.datamodel.PerformansHedefi;

public interface FaaliyetService {
	
	
	public List<Faaliyet> getAllFaaliyet();
	public List<Faaliyet> getByPerformansHedefi(PerformansHedefi performansHedefi);
	public List<Faaliyet> getByDepartment(Long id);
	public Faaliyet getById(Long id);
	public void updateFaaliyet(Faaliyet faaliyet) throws Exception;
	public void deleteFaaliyet(Faaliyet faaliyet) throws Exception;
	public void addFaaliyet(Faaliyet faaliyet) throws Exception;
	

}
