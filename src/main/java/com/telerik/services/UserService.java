package com.telerik.services;

import java.util.List;

import com.telerik.datamodel.Department;
import com.telerik.datamodel.UserInfo;

public interface UserService {
	
	public void addUser(UserInfo user);
	public void editUser(UserInfo user);
	public boolean deleteUser(Long id);
	public boolean disActiveUser(Long id);
	public boolean activeUser(Long id);
	public UserInfo getUser(String username);
	public List<UserInfo> getUsersByDept(Department department);
	public List<UserInfo> getAllUser();
}
