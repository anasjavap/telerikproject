package com.telerik.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.Budget;
import com.telerik.datamodel.Faaliyet;
import com.telerik.services.BudgetService;
import com.telerik.services.FaaliyetService;

@Controller
@RequestMapping(value ="/budget/")
public class BudgetController {

	@Autowired
	private BudgetService budgetService;
	@Autowired
	private FaaliyetService faaliyetService; 
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<Budget> read(@RequestBody Map<String, Object> model) {
		Long id = Long.valueOf(model.get("faaliyetId").toString());
		Faaliyet faaliyet = faaliyetService.getById(id);
		return budgetService.getByFaaliyet(faaliyet);
	
	}

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody Budget budget) {
		budget.setToplamButce(budget.getBorc() + budget.getCari()+budget.getFaiz()+ budget.getMalVeHizmat()+budget.getPersonalGider()+budget.getSermaye() + budget.getSermayeTransfer() + budget.getSgk());
		budget.setToplamDisi(budget.getDigerYurt() + budget.getDonerSermaye() + budget.getYurt());
		budget.setToplamKaynek(budget.getToplamButce() + budget.getToplamDisi());
		budgetService.updateBudget(budget);
		Gson gson = new Gson();
		return gson.toJson(budget);
	}

	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody Budget budget) {
		budgetService.deleteBudget(budget);
		Gson gson = new Gson();
		return gson.toJson(budget);
	}

	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody Map<String, Object> model) {
		for (String key : model.keySet()) {
			System.err.println(key +"----"+ model.get(key));
		}
		Budget budget = new Budget();
		Long id = Long.valueOf(model.get("faaliyetId").toString());
		Faaliyet faaliyet = faaliyetService.getById(id);
		budget.setFaaliyet(faaliyet);
		budget.setAceklamaler(model.get("aciklamaler").toString());	
		budget.setPersonalGider(Double.valueOf(model.get("personalGider").toString()));	
		budget.setSgk(Double.valueOf(model.get("sgk").toString()));	
		budget.setMalVeHizmat(Double.valueOf(model.get("malVeHizmat").toString()));	
		budget.setFaiz(Double.valueOf(model.get("faiz").toString()));	
		budget.setCari(Double.valueOf(model.get("cari").toString()));	
		budget.setSermaye(Double.valueOf(model.get("sermaye").toString()));	
		budget.setSermayeTransfer(Double.valueOf(model.get("sermayeTransfer").toString()));	
		budget.setBorc(Double.valueOf(model.get("borc").toString()));	
		budget.setDonerSermaye(Double.valueOf(model.get("donerSermaye").toString()));	
		budget.setDigerYurt(Double.valueOf(model.get("digerYurt").toString()));	
		budget.setYurt(Double.valueOf(model.get("yurt").toString()));	
		budget.setToplamButce(Double.valueOf(model.get("toplamButce").toString()));	
		budget.setToplamDisi(Double.valueOf(model.get("toplamDisi").toString()));	
		budget.setToplamKaynek(Double.valueOf(model.get("toplamKaynek").toString()));	
		budget.setToplamButce(budget.getBorc() + budget.getCari()+budget.getFaiz()+ budget.getMalVeHizmat()+budget.getPersonalGider()+budget.getSermaye() + budget.getSermayeTransfer() + budget.getSgk());
		budget.setToplamDisi(budget.getDigerYurt() + budget.getDonerSermaye() + budget.getYurt());
		budget.setToplamKaynek(budget.getToplamButce() + budget.getToplamDisi());
		budgetService.addBudget(budget);
		Gson gson = new Gson();
		return gson.toJson(budget);
	}


	public BudgetService getBudgetService() {
		return budgetService;
	}


	public void setBudgetService(BudgetService budgetService) {
		this.budgetService = budgetService;
	}


	public FaaliyetService getFaaliyetService() {
		return faaliyetService;
	}


	public void setFaaliyetService(FaaliyetService faaliyetService) {
		this.faaliyetService = faaliyetService;
	}
	
	

}
