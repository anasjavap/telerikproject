package com.telerik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.GostergeOlcumSekil;
import com.telerik.services.GostergeOlcumSekilService;

@Controller
@RequestMapping(value = "olcum")
public class OlcumSikelController {

	@Autowired
	private GostergeOlcumSekilService gostergeOlcumSekilService;

	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public @ResponseBody List<GostergeOlcumSekil> read() {
		return gostergeOlcumSekilService.getAllOlcum();
	}

	

	@RequestMapping(value = "/readall", method = RequestMethod.POST)
	public @ResponseBody List<GostergeOlcumSekil> readall() {
		return gostergeOlcumSekilService.getAllOlcum();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody GostergeOlcumSekil olcumSekil) {
		try {
			gostergeOlcumSekilService.updateOclum(olcumSekil);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(olcumSekil);

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody GostergeOlcumSekil olcumSekil) {
		try {
			gostergeOlcumSekilService.addOclum(olcumSekil);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(olcumSekil);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody GostergeOlcumSekil olcumSekil) {
		try {
			gostergeOlcumSekilService.deleteOclum(olcumSekil);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(olcumSekil);
	}



	public GostergeOlcumSekilService getGostergeOlcumSekilService() {
		return gostergeOlcumSekilService;
	}



	public void setGostergeOlcumSekilService(GostergeOlcumSekilService gostergeOlcumSekilService) {
		this.gostergeOlcumSekilService = gostergeOlcumSekilService;
	}

}
