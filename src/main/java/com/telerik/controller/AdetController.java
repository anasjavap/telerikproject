package com.telerik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.GostergeAdet;
import com.telerik.services.GostergeAdetService;

@Controller
@RequestMapping(value = "/adet/")
public class AdetController {

	@Autowired
	private GostergeAdetService adetService;

	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public @ResponseBody List<GostergeAdet> read() {
		return adetService.getAllAdet();
	}

	@RequestMapping(value = "/readall", method = RequestMethod.POST)
	public @ResponseBody List<GostergeAdet> readall() {
		return adetService.getAllAdet();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody GostergeAdet adet) {
		try {
			adetService.updateAdet(adet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(adet);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody GostergeAdet adet) {
		try {
			adetService.addAdet(adet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();

		return gson.toJson(adet);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody GostergeAdet adet) {
		try {
			adetService.deleteAdet(adet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();

		return gson.toJson(adet);
	}

	public GostergeAdetService getAdetService() {
		return adetService;
	}

	public void setAdetService(GostergeAdetService adetService) {
		this.adetService = adetService;
	}
	
	

}
