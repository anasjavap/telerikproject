package com.telerik.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.Department;
import com.telerik.datamodel.UserInfo;
import com.telerik.services.DeptService;
import com.telerik.services.RolesService;
import com.telerik.services.UserService;

@Controller
@RequestMapping(value = "/user/")
public class ManagmentController {

	
	@Autowired
	private UserService userService;
	@Autowired
	private DeptService deptService;
	@Autowired
	private RolesService rolesService;
	
	private Department selectedDept;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String managment(){
		return "managment";
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody Map<String,Object> model) {
		UserInfo userInfo =  new UserInfo();
		if (selectedDept != null) {
			userInfo.setFirstName(model.get("firstName").toString());
			userInfo.setLastName(model.get("lastName").toString());
			userInfo.setPassword(model.get("password").toString());
			userInfo.setAddress(model.get("address").toString());
			userInfo.setPhone(model.get("phone").toString());
			userInfo.setUsername(model.get("username").toString());
			userInfo.setDepartment(selectedDept);
			userInfo.setRole(rolesService.getRoleById(Long.valueOf(model.get("role").toString())));
			userInfo.setActive(Boolean.valueOf(model.get("active").toString()));
			userService.addUser(userInfo);
		}
	
		Gson gson = new Gson();
		return gson.toJson(userInfo);
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody UserInfo user) {
		user.setDepartment(selectedDept);
		userService.editUser(user);
		Gson gson = new Gson();
		return gson.toJson(user);
	}
	
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<UserInfo> read(@RequestParam("id") String id) {
		selectedDept = deptService.getDeptbyId(Long.valueOf(id));
		return userService.getUsersByDept(selectedDept);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody UserInfo user) {
		userService.deleteUser(user.getId());
		Gson gson = new Gson();
		return gson.toJson(user);
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public DeptService getDeptService() {
		return deptService;
	}

	public void setDeptService(DeptService deptService) {
		this.deptService = deptService;
	}

	public RolesService getRolesService() {
		return rolesService;
	}

	public void setRolesService(RolesService rolesService) {
		this.rolesService = rolesService;
	}

	public Department getSelectedDept() {
		return selectedDept;
	}

	public void setSelectedDept(Department selectedDept) {
		this.selectedDept = selectedDept;
	}
	
	
}
