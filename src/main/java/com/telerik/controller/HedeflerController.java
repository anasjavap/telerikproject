package com.telerik.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.StrategyPlanHedefler;
import com.telerik.services.StrategyPlanAmaclarService;
import com.telerik.services.StrategyPlanHedeflerService;

@Controller
@RequestMapping(value = "/hedef/")
public class HedeflerController {

	@Autowired
	private StrategyPlanHedeflerService hedeflerService;
	@Autowired
	private StrategyPlanAmaclarService amaclarService; 

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<StrategyPlanHedefler> readHedefler(@RequestBody Map<String, Object> model) {
		String id =model.get("amacId").toString();
		if (!id.equals("")) {
	    	return hedeflerService.getHedeflerByAmacId(Long.valueOf(id));
		}else{
			return new ArrayList<StrategyPlanHedefler>();
		}
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody String create(@RequestBody Map<String, Object> model) {		
		StrategyPlanHedefler hedef =  new StrategyPlanHedefler();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		try {
			Date date = formatter.parse(model.get("createDate").toString());
			hedef.setCreateDate(date);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		hedef.setAckiama(model.get("ackiama").toString());
		hedef.setHedef(model.get("hedef").toString());
		hedef.setOrderIndex(Integer.valueOf(model.get("orderIndex").toString()));
	    hedef.setStatus(Integer.valueOf(model.get("status").toString()));
		hedef.setAmaclar(amaclarService.getAmacById(Long.valueOf(model.get("amacId").toString())));
		hedeflerService.addHedef(hedef);
		Gson gson = new Gson();
		return gson.toJson(hedef);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody StrategyPlanHedefler hedef) {
		try {
			hedeflerService.updateHedef(hedef);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(hedef);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody StrategyPlanHedefler hedef) {
		try {
			hedeflerService.deleteHedef(hedef);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(hedef);
	}
	
	
	public StrategyPlanHedeflerService getHedeflerService() {
		return hedeflerService;
	}

	public void setHedeflerService(StrategyPlanHedeflerService hedeflerService) {
		this.hedeflerService = hedeflerService;
	}

	public StrategyPlanAmaclarService getAmaclarService() {
		return amaclarService;
	}

	public void setAmaclarService(StrategyPlanAmaclarService amaclarService) {
		this.amaclarService = amaclarService;
	}

	
}
