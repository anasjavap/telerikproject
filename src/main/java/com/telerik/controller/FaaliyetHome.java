package com.telerik.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="faaliyetHome")
public class FaaliyetHome {

	@RequestMapping(value ="/" , method = RequestMethod.GET)
	public String index(){
		return "faaliyetHome";
	}
}
