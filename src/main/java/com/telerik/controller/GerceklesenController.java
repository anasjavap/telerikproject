package com.telerik.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.Department;
import com.telerik.datamodel.Gerceklesen;
import com.telerik.datamodel.Quarter;
import com.telerik.datamodel.StrategyPlanGostergeler;
import com.telerik.datamodel.UserInfo;
import com.telerik.services.GerceklesenService;
import com.telerik.services.StrategyPlanGostergelerService;
import com.telerik.services.UserService;

@Controller
@RequestMapping(value = "/gerceklesen/")
public class GerceklesenController {

	@Autowired
	private GerceklesenService gerceklesenService;
	@Autowired
	private StrategyPlanGostergelerService gostergelerService;
	@Autowired
	private UserService userservice;

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	private @ResponseBody List<Gerceklesen> read(@RequestBody Map<String, Object> model) {
		String id = model.get("gostergeId").toString();
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();
		UserInfo userInfo = userservice.getUser(name);
		StrategyPlanGostergeler gostergeler = gostergelerService.getById(Long.valueOf(id));
		if (userInfo.getRole().getName().equals("admin")) {
			return gerceklesenService.getByGosterge(gostergeler);
		} else {
			List<Department> departments = gostergeler.getPerformansHedefi().getHedef().getAmaclar().getDepartment();
			Department dept = userInfo.getDepartment();
			for (Department department : departments) {
				if (department.equals(dept)) {
					return gerceklesenService.getByGosterge(gostergeler);
				}
			}

		}
		return new ArrayList<Gerceklesen>();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	private @ResponseBody String update(@RequestBody Gerceklesen gerceklesen) {
		try {
			if (gerceklesen.getGostergeler().getGostergelerTur().getAdi().equals("sum")) {
				int r1 = gerceklesen.getQuarter1().getRealValue();
				int r2 = gerceklesen.getQuarter2().getRealValue();
				int r3 = gerceklesen.getQuarter3().getRealValue();
				int r4 = gerceklesen.getQuarter4().getRealValue();
				int x = r1 + r2 + r3 + r4;
				gerceklesen.setToplam(String.valueOf(x));
			}
			gerceklesenService.updateGerceklesen(gerceklesen);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(gerceklesen);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	private @ResponseBody String create(@RequestBody Map<String, Object> model) {

		Gerceklesen gerceklesen = new Gerceklesen();
		try {

			Quarter quarter1 = new Quarter();
			Quarter quarter2 = new Quarter();
			Quarter quarter3 = new Quarter();
			Quarter quarter4 = new Quarter();

			JSONObject q1 = new JSONObject(model.get("quarter1").toString().replace("=", ":"));
			JSONObject q2 = new JSONObject(model.get("quarter2").toString().replace("=", ":"));
			JSONObject q3 = new JSONObject(model.get("quarter3").toString().replace("=", ":"));
			JSONObject q4 = new JSONObject(model.get("quarter4").toString().replace("=", ":"));

			quarter1.setRealValue(q1.getInt("realValue"));
			quarter2.setRealValue(q2.getInt("realValue"));
			quarter3.setRealValue(q3.getInt("realValue"));
			quarter4.setRealValue(q4.getInt("realValue"));

			quarter1.setTargetValue(q1.getInt("targetValue"));
			quarter2.setTargetValue(q2.getInt("targetValue"));
			quarter3.setTargetValue(q3.getInt("targetValue"));
			quarter4.setTargetValue(q4.getInt("targetValue"));

			gerceklesen.setQuarter1(quarter1);
			gerceklesen.setQuarter2(quarter2);
			gerceklesen.setQuarter3(quarter3);
			gerceklesen.setQuarter4(quarter4);

			gerceklesen.setYil(Integer.valueOf(model.get("yil").toString()));

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = formatter.parse(model.get("createdDate").toString());
			gerceklesen.setCreatedDate(date);

			gerceklesen.setGostergeler(gostergelerService.getById(Long.valueOf(model.get("gostergeId").toString())));

			gerceklesen.setToplam(model.get("toplam").toString());

			gerceklesenService.addGerceklesen(gerceklesen);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(gerceklesen);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	private @ResponseBody String delete(@RequestBody Gerceklesen gerceklesen) {
		try {
			gerceklesenService.deleteGerceklesen(gerceklesen);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(gerceklesen);
	}

	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public String canEdit(@RequestParam("gostId") String gostId) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();
		StrategyPlanGostergeler gostergeler = gostergelerService.getById(Long.valueOf(gostId));
		UserInfo userInfo = userservice.getUser(name);
		if (userInfo.getRole().getName().equals("admin")) {
			return "true";
		} else {
			List<Department> departments = gostergeler.getPerformansHedefi().getHedef().getAmaclar().getDepartment();
			Department dept = userInfo.getDepartment();
			for (Department department : departments) {
				if (department.equals(dept)) {
					return "true";
				}
			}
		}
		return "false";
	}

	public GerceklesenService getGerceklesenService() {
		return gerceklesenService;
	}

	public void setGerceklesenService(GerceklesenService gerceklesenService) {
		this.gerceklesenService = gerceklesenService;
	}

	public StrategyPlanGostergelerService getGostergelerService() {
		return gostergelerService;
	}

	public void setGostergelerService(StrategyPlanGostergelerService gostergelerService) {
		this.gostergelerService = gostergelerService;
	}

	public UserService getUserservice() {
		return userservice;
	}

	public void setUserservice(UserService userservice) {
		this.userservice = userservice;
	}
	
	

}
