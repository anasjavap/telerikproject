package com.telerik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.telerik.datamodel.Maliyeti;
import com.telerik.services.MaliyetiService;

@Controller
@RequestMapping(value ="/maliyeti/")
public class MaliyetiController {
	
	@Autowired
	private MaliyetiService maliyetiService;
	
	@RequestMapping(value = "/read" , method = RequestMethod.POST)
	public @ResponseBody List<Maliyeti> read(){
		return maliyetiService.getAllMaliyeti();
	}

	public MaliyetiService getMaliyetiService() {
		return maliyetiService;
	}

	public void setMaliyetiService(MaliyetiService maliyetiService) {
		this.maliyetiService = maliyetiService;
	}

}
