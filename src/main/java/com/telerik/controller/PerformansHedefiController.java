package com.telerik.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanHedefler;
import com.telerik.services.PerformansHedefiService;
import com.telerik.services.StrategyPlanHedeflerService;

@Controller
@RequestMapping(value = "/perfhedef/")
public class PerformansHedefiController {

	
	@Autowired 
	private PerformansHedefiService performansHedefiService;
	@Autowired
	private StrategyPlanHedeflerService hedeflerService; 
	
	//private Long id;
	private StrategyPlanHedefler hedef;
	
	@RequestMapping(value ="/read" ,method= RequestMethod.POST)
	public @ResponseBody List<PerformansHedefi> read(@RequestBody Map<String, Object> model){
		String id  = model.get("hedefId").toString();
		if (!id.equals("")) {
			hedef=hedeflerService.getHedefById(Long.valueOf(id));
			return performansHedefiService.getByHedef(hedef);
		}else{
			return new ArrayList<PerformansHedefi>();
		}
	
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(@RequestBody PerformansHedefi performans) {
		try {
			performansHedefiService.updatePerformansHedef(performans);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(performans);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(@RequestBody PerformansHedefi performans) {
		try {
			performansHedefiService.deletePerformansHedef(performans);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(performans);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody String create(@RequestBody Map<String, Object> model) {
		PerformansHedefi performans =new PerformansHedefi();
		try {
			performans.setAciklama(model.get("aciklama").toString());
			performans.setAdi(model.get("adi").toString());
			performans.setHedef(hedeflerService.getHedefById(Long.valueOf(model.get("hedefId").toString())));
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = formatter.parse(model.get("createdDate").toString());
			performans.setCreatedDate(date);
			performansHedefiService.addPerformansHedef(performans);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}Gson gson = new Gson();
		return gson.toJson(performans);
	}

	public PerformansHedefiService getPerformansHedefiService() {
		return performansHedefiService;
	}

	public void setPerformansHedefiService(PerformansHedefiService performansHedefiService) {
		this.performansHedefiService = performansHedefiService;
	}

	public StrategyPlanHedeflerService getHedeflerService() {
		return hedeflerService;
	}

	public void setHedeflerService(StrategyPlanHedeflerService hedeflerService) {
		this.hedeflerService = hedeflerService;
	}

	public StrategyPlanHedefler getHedef() {
		return hedef;
	}

	public void setHedef(StrategyPlanHedefler hedef) {
		this.hedef = hedef;
	}
		
		
		
	
}
