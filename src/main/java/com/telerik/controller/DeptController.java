package com.telerik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.Department;
import com.telerik.services.DeptService;
import com.telerik.services.UserService;

@Transactional
@Controller
@RequestMapping(value = "/dept/")
public class DeptController {

	@Autowired
	private DeptService deptService;
	@Autowired
	private UserService userService;

//	@RequestMapping(value = "/read", method = RequestMethod.POST)
//	public @ResponseBody List<Department> read(@RequestBody Map<String, Object> model) {
//		List<Department> dept=  new ArrayList<Department>();
//		String username =model.get("username").toString();
//		if(!username.equals("")){
//			UserInfo user= userService.getUser(username);
//			if (user.getRole().getName().equals("admin")) {
//				return deptService.getAllDept();
//			}else if (user.getDepartment() != null) {
//				dept.add(user.getDepartment());
//				return dept;
//			}
//		}
//		return dept;
//	}

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<Department> readall() {
		return deptService.getAllDept();
	}
	
	@RequestMapping(value = "/readall", method = RequestMethod.GET)
	public @ResponseBody List<Department> read() {
		return deptService.getAllDept();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody Department dept) {
		deptService.updateDept(dept);
		 Gson gson = new Gson();
		 return gson.toJson(dept);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody Department department) {
		deptService.addDept(department);
		 Gson gson = new Gson();
		 return gson.toJson(department);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody Department dept) {
		try{
			deptService.deleteDept(dept);
		}catch(Exception e){
			e.printStackTrace();
		}
		 Gson gson = new Gson();
		 return gson.toJson(dept);
	}

	public DeptService getDeptService() {
		return deptService;
	}

	public void setDeptService(DeptService deptService) {
		this.deptService = deptService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
