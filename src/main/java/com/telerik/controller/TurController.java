package com.telerik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.GostergelerTur;
import com.telerik.services.GostergelerTurService;

@Controller
@RequestMapping(value ="/tur/")
public class TurController {
	
	@Autowired
	private GostergelerTurService turService;
	
	@RequestMapping(value ="/read" , method =RequestMethod.GET)
	public @ResponseBody List<GostergelerTur> readget(){
		return turService.getAllTur();
	}
	
	@RequestMapping(value ="/readall" , method =RequestMethod.POST)
	public @ResponseBody List<GostergelerTur> readall(){
		return turService.getAllTur();
	}
	
	
	@RequestMapping(value ="/update" , method =RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody GostergelerTur tur){
		try {
			turService.updateTue(tur);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(tur);
	}
	
	@RequestMapping(value ="/delete" , method =RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody GostergelerTur tur){
		try {
			turService.deleteTue(tur);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(tur);
	}
	
	
	@RequestMapping(value ="/create" , method =RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody GostergelerTur tur){
		try {
			turService.addTue(tur);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(tur);
	}

	public GostergelerTurService getTurService() {
		return turService;
	}

	public void setTurService(GostergelerTurService turService) {
		this.turService = turService;
	}

}
