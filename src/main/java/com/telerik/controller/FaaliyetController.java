package com.telerik.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.Faaliyet;
import com.telerik.datamodel.PerformansHedefi;
import com.telerik.services.FaaliyetService;
import com.telerik.services.PerformansHedefiService;

@Controller
@RequestMapping(value = "/faaliyet/")
public class FaaliyetController {

	@Autowired
	private FaaliyetService faaliyetService;
	@Autowired
	private PerformansHedefiService perHedefiService;

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<Faaliyet> read(@RequestBody Map<String, Object> model) {
		Long id = Long.valueOf(model.get("perfId").toString());
		PerformansHedefi performansHedefi = perHedefiService.getById(id);
		return faaliyetService.getByPerformansHedefi(performansHedefi);
	}

	@RequestMapping(value = "/readForHome", method = RequestMethod.POST)
	public @ResponseBody List<Faaliyet> readForHome(@RequestParam("id") String id) {
		//String deptId = model.get("deptId").toString();
		if (id.equals("")) {
			return faaliyetService.getAllFaaliyet();
		} else {
			return faaliyetService.getByDepartment(Long.valueOf(id));
		}
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestBody Faaliyet faaliyet) {
		try {
			faaliyetService.updateFaaliyet(faaliyet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(faaliyet);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody Faaliyet faaliyet) {
		try {
			faaliyetService.deleteFaaliyet(faaliyet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(faaliyet);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody Map<String, Object> model) {
		for (String key : model.keySet()) {
			System.err.println(key + "=====" + model.get(key));
		}
		Faaliyet faaliyet = new Faaliyet();
		try {
			Long id = Long.valueOf(model.get("perfId").toString());
			PerformansHedefi performansHedefi = perHedefiService.getById(id);
			faaliyet.setPerformansHedefi(performansHedefi);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date basangicTarihi = formatter.parse(model.get("basangicTarihi").toString());
			faaliyet.setBasangicTarihi(basangicTarihi);
			Date bitisTarihi = formatter.parse(model.get("bitisTarihi").toString());
			faaliyet.setBitisTarihi(bitisTarihi);
			Date createdDate = formatter.parse(model.get("createdDate").toString());
			faaliyet.setCreatedDate(createdDate);
			faaliyet.setFaaliyetAciklama(model.get("faaliyetAciklama").toString());
			faaliyet.setFaaliyetAdi(model.get("faaliyetAdi").toString());
			faaliyet.setSurezaman(model.get("surezaman").toString());
			faaliyet.setSurezamanTuri(model.get("surezamanTuri").toString());
			faaliyet.setStatus(Integer.valueOf(model.get("status").toString()));
			faaliyetService.addFaaliyet(faaliyet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(faaliyet);
	}

	public FaaliyetService getFaaliyetService() {
		return faaliyetService;
	}

	public void setFaaliyetService(FaaliyetService faaliyetService) {
		this.faaliyetService = faaliyetService;
	}

	public PerformansHedefiService getPerHedefiService() {
		return perHedefiService;
	}

	public void setPerHedefiService(PerformansHedefiService perHedefiService) {
		this.perHedefiService = perHedefiService;
	}

}
