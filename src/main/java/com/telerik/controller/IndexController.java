package com.telerik.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/index/")
public class IndexController {

	@RequestMapping(value= "/" , method= RequestMethod.GET)
	public String index(){
		return "index";
	}

	
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody void save(String fileName, String base64, String contentType, HttpServletResponse response) throws IOException {

        response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
        response.setContentType(contentType);
        byte[] data = DatatypeConverter.parseBase64Binary(base64);
        response.setContentLength(data.length);
        response.getOutputStream().write(data);
        response.flushBuffer();
    }


}
