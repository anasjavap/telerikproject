package com.telerik.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.Department;
import com.telerik.datamodel.StrategyPlanAmaclar;
import com.telerik.services.DeptService;
import com.telerik.services.StrategyPlanAmaclarService;

@Controller
@RequestMapping(value = "/amac/")
public class AmaclarController {
	
	@Autowired
	private StrategyPlanAmaclarService amaclarService;
	@Autowired
	private DeptService deptService;
	
	private Department  selectedDept;

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<StrategyPlanAmaclar> read(@RequestParam("id") String id) {
		if(!id.equals("")){
			selectedDept = deptService.getDeptbyId(Long.valueOf(id));
			return amaclarService.getByDept(selectedDept.getDeptId());
		}else{
			return new ArrayList<StrategyPlanAmaclar>();
		}
		
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody StrategyPlanAmaclar amaclar) {		
		 amaclarService.addAmac(amaclar);
		 Gson gson = new Gson();
		 return gson.toJson(amaclar);
	}
		
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public  @ResponseBody String update(@RequestBody StrategyPlanAmaclar amac) {
		amaclarService.updateAmac(amac);
		 Gson gson = new Gson();
		 return gson.toJson(amac);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String deleteAmac(@RequestBody StrategyPlanAmaclar amac) {
		if (amac.getDepartment().size() > 1) {
			for (Department depts : amac.getDepartment()) {
				if (selectedDept.getDeptId().equals(depts.getDeptId())) {
					amac.getDepartment().remove(depts);
					amaclarService.updateAmac(amac);				
				}
			}
		}else{
			try {
				amaclarService.deleteAmac(amac);
				
			} catch (Exception e) {
				e.printStackTrace();
			}				
		}
		 Gson gson = new Gson();
		 return gson.toJson(amac);
	}
	
	
	public StrategyPlanAmaclarService getAmaclarService() {
		return amaclarService;
	}

	public void setAmaclarService(StrategyPlanAmaclarService amaclarService) {
		this.amaclarService = amaclarService;
	}


	public DeptService getDeptService() {
		return deptService;
	}


	public void setDeptService(DeptService deptService) {
		this.deptService = deptService;
	}


	public Department getSelectedDept() {
		return selectedDept;
	}


	public void setSelectedDept(Department selectedDept) {
		this.selectedDept = selectedDept;
	}
	

}
