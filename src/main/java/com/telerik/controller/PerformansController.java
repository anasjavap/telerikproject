package com.telerik.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.Performans;
import com.telerik.datamodel.StrategyPlanHedefler;
import com.telerik.services.PerformansService;
import com.telerik.services.StrategyPlanHedeflerService;

@Controller
@RequestMapping(value = "/performans/")
public class PerformansController {

	@Autowired
	private PerformansService performansService;
	@Autowired
	private StrategyPlanHedeflerService hedeflerService;

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<Performans> read(@RequestBody Map<String, Object> model) {
		String id = model.get("hedefId").toString();
		if(!id.equals("")){
			StrategyPlanHedefler hedef=hedeflerService.getHedefById(Long.valueOf(id));
			return performansService.getByHedef(hedef);
		}else{
			return new ArrayList<Performans>();
		}
		
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(@RequestBody Performans performans) {
		try {
			performansService.updatePerformans(performans);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		
		}
		Gson gson = new Gson();
		return gson.toJson(performans);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody String create(@RequestBody Map<String, Object> model) {
		Performans performans =  new Performans();
		try {
			
			performans.setOrederIndex(Integer.valueOf(model.get("orederIndex").toString()));
			performans.setStatus(Integer.valueOf(model.get("status").toString()));
			performans.setMevcutdurum(model.get("mevcutdurum").toString());
			performans.setHedefler(hedeflerService.getHedefById(Long.valueOf(model.get("hedefId").toString())));
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = formatter.parse(model.get("createdDate").toString());
			performans.setCreatedDate(date);
			performansService.addPerformans(performans);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		Gson gson = new Gson();
		return gson.toJson(performans);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody Performans performans) {
		try {
			performansService.deletePerformans(performans);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson gson = new Gson();
		return gson.toJson(performans);
	}

	public PerformansService getPerformansService() {
		return performansService;
	}

	public void setPerformansService(PerformansService performansService) {
		this.performansService = performansService;
	}

	public StrategyPlanHedeflerService getHedeflerService() {
		return hedeflerService;
	}

	public void setHedeflerService(StrategyPlanHedeflerService hedeflerService) {
		this.hedeflerService = hedeflerService;
	}

}
