package com.telerik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.telerik.datamodel.Role;
import com.telerik.services.RolesService;

@Controller
@RequestMapping(value ="/role/")
public class RoleController {

	@Autowired
	private RolesService rolesService;
	
	@RequestMapping(value="/read" ,method = RequestMethod.GET)
	public @ResponseBody List<Role> read(){
		return rolesService.getAllRoles();
	}

	public RolesService getRolesService() {
		return rolesService;
	}

	public void setRolesService(RolesService rolesService) {
		this.rolesService = rolesService;
	}
}
