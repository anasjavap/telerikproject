package com.telerik.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telerik.datamodel.GostergeAdet;
import com.telerik.datamodel.GostergeOlcumSekil;
import com.telerik.datamodel.GostergelerTur;
import com.telerik.datamodel.PerformansHedefi;
import com.telerik.datamodel.StrategyPlanGostergeler;
import com.telerik.services.GostergeAdetService;
import com.telerik.services.GostergeOlcumSekilService;
import com.telerik.services.GostergelerTurService;
import com.telerik.services.PerformansHedefiService;
import com.telerik.services.StrategyPlanGostergelerService;

@Controller
@RequestMapping(value = "/gosterge/")
public class GostergelerController {

	@Autowired
	private StrategyPlanGostergelerService gostergelerService;
	@Autowired
	private  PerformansHedefiService performansHedefiService; 
	@Autowired
	private GostergeAdetService adetService;
	@Autowired 
	private GostergeOlcumSekilService olcumSekilService;
	@Autowired
	private GostergelerTurService turService;
	
	//private Long id;
	
	private PerformansHedefi performansHedefi;
	
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody List<StrategyPlanGostergeler> read(@RequestBody Map<String, Object> model) {
		String id= model.get("perfId").toString();
		if (!id.equals("")) {
			performansHedefi=performansHedefiService.getById(Long.valueOf(id));
		return gostergelerService.getByPerformans(performansHedefi);
		}else{
			return new ArrayList<StrategyPlanGostergeler>();
		}
	}
	
	
	
	@RequestMapping(value = "/readForHome", method = RequestMethod.POST)
	public @ResponseBody List<StrategyPlanGostergeler> readForHome(@RequestParam("id") String id) {
		if (id.equals("")) {
		    return gostergelerService.getAllGosterge();
		}else{
			return gostergelerService.getByDepartment(Long.valueOf(id));
		}
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(@RequestBody StrategyPlanGostergeler gosterge) {
		gostergelerService.addGosterge(gosterge);
		Gson gson = new Gson();
		return gson.toJson(gosterge);
	}
	

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestBody Map<String,Object> model) {
		StrategyPlanGostergeler gosterge = new StrategyPlanGostergeler();
	try {
	    
		GostergelerTur tur = turService.getById(Long.valueOf(model.get("gostergelerTur").toString()));
		GostergeAdet adet = adetService.getById(Long.valueOf(model.get("adetcCinsi").toString()));
		GostergeOlcumSekil olcum= olcumSekilService.getById(Long.valueOf(model.get("oclumsiklik").toString()));
		PerformansHedefi performansHedefi = performansHedefiService.getById(Long.valueOf(model.get("perfId").toString()));
		gosterge.setGostergelerTur(tur);
		gosterge.setOclumsiklik(olcum);
		gosterge.setAdetcCinsi(adet);
		gosterge.setPerformansHedefi(performansHedefi);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Date date = formatter.parse(model.get("createDate").toString());
		gosterge.setCreateDate(date);
		gosterge.setAckiama(model.get("ackiama").toString());
		gosterge.setAdi(model.get("adi").toString());
		gosterge.setOrderIndex(Integer.valueOf(model.get("orderIndex").toString()));
		gosterge.setStatus(Integer.valueOf(model.get("status").toString()));
		gostergelerService.addGosterge(gosterge);	
		
		} catch (ParseException e) {
			e.printStackTrace();
		
		}
		Gson gson = new Gson();
		return gson.toJson(gosterge);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object delete(@RequestBody StrategyPlanGostergeler gosterge) {
		try {
			gostergelerService.deleteGosterge(gosterge);
		} catch (Exception e) {
		}
		Gson gson = new Gson();
		return gson.toJson(gosterge);

	}

	public StrategyPlanGostergelerService getGostergelerService() {
		return gostergelerService;
	}

	public void setGostergelerService(StrategyPlanGostergelerService gostergelerService) {
		this.gostergelerService = gostergelerService;
	}

	public PerformansHedefiService getPerformansHedefiService() {
		return performansHedefiService;
	}

	public void setPerformansHedefiService(PerformansHedefiService performansHedefiService) {
		this.performansHedefiService = performansHedefiService;
	}

	public GostergeAdetService getAdetService() {
		return adetService;
	}

	public void setAdetService(GostergeAdetService adetService) {
		this.adetService = adetService;
	}

	public GostergeOlcumSekilService getOlcumSekilService() {
		return olcumSekilService;
	}

	public void setOlcumSekilService(GostergeOlcumSekilService olcumSekilService) {
		this.olcumSekilService = olcumSekilService;
	}

	public GostergelerTurService getTurService() {
		return turService;
	}

	public void setTurService(GostergelerTurService turService) {
		this.turService = turService;
	}

	public PerformansHedefi getPerformansHedefi() {
		return performansHedefi;
	}

	public void setPerformansHedefi(PerformansHedefi performansHedefi) {
		this.performansHedefi = performansHedefi;
	}

	
	
}
