package com.telerik.datamodel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class StrategyPlanAmaclar {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long amacId;
	
	private String amac;
	private String status;
	
	@Temporal(TemporalType.DATE)
	private Date createDate;
	private int	OrderIndex;
	private String ackiama;
		
	@ManyToMany(fetch = FetchType.EAGER,cascade=CascadeType.MERGE)
	private List<Department> department =  new ArrayList<Department>();
	
	public Long getAmacId() {
		return amacId;
	}
	public void setAmacId(Long amacId) {
		this.amacId = amacId;
	}
	public String getAmac() {
		return amac;
	}
	public void setAmac(String amac) {
		this.amac = amac;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getOrderIndex() {
		return OrderIndex;
	}
	public void setOrderIndex(int orderIndex) {
		OrderIndex = orderIndex;
	}
	public String getAckiama() {
		return ackiama;
	}
	public void setAckiama(String ackiama) {
		this.ackiama = ackiama;
	}
	public List<Department> getDepartment() {
		return department;
	}
	public void setDepartment(List<Department> department) {
		this.department = department;
	}	
}
