package com.telerik.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GostergelerTur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long turId;
	
	private String adi;

	public Long getTurId() {
		return turId;
	}

	public void setTurId(Long turId) {
		this.turId = turId;
	}

	public String getAdi() {
		return adi;
	}

	public void setAdi(String adi) {
		this.adi = adi;
	}
	

}
