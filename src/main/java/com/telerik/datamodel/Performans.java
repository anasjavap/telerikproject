package com.telerik.datamodel;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Performans {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long perId;
	
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	
	private int status;
	private int orederIndex;
	
	@ManyToOne
	private StrategyPlanHedefler hedefler;
	
	private String mevcutdurum;

	public Long getPerId() {
		return perId;
	}

	public void setPerId(Long perId) {
		this.perId = perId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getOrederIndex() {
		return orederIndex;
	}

	public void setOrederIndex(int orederIndex) {
		this.orederIndex = orederIndex;
	}

	public String getMevcutdurum() {
		return mevcutdurum;
	}

	public void setMevcutdurum(String mevcutdurum) {
		this.mevcutdurum = mevcutdurum;
	}

	public StrategyPlanHedefler getHedefler() {
		return hedefler;
	}

	public void setHedefler(StrategyPlanHedefler hedefler) {
		this.hedefler = hedefler;
	}
	
	
	
}
