package com.telerik.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GostergeOlcumSekil {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long olcumId;
	
	private String olcumSekil;

	public Long getOlcumId() {
		return olcumId;
	}

	public void setOlcumId(Long olcumId) {
		this.olcumId = olcumId;
	}

	public String getOlcumSekil() {
		return olcumSekil;
	}

	public void setOlcumSekil(String olcumSekil) {
		this.olcumSekil = olcumSekil;
	}

}
