package com.telerik.datamodel;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Faaliyet {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long faaliyetId;
	
	private String faaliyetAdi;
	private int status;
	private String faaliyetAciklama;
	private String surezaman;
	private String surezamanTuri;
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	@Temporal(TemporalType.DATE)
	private Date basangicTarihi;
	@Temporal(TemporalType.DATE)
	private Date bitisTarihi;
	
	@ManyToOne
	private PerformansHedefi performansHedefi;
	
	public Long getFaaliyetId() {
		return faaliyetId;
	}
	public void setFaaliyetId(Long faaliyetId) {
		this.faaliyetId = faaliyetId;
	}
	public String getFaaliyetAdi() {
		return faaliyetAdi;
	}
	public void setFaaliyetAdi(String faaliyetAdi) {
		this.faaliyetAdi = faaliyetAdi;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getFaaliyetAciklama() {
		return faaliyetAciklama;
	}
	public void setFaaliyetAciklama(String faaliyetAciklama) {
		this.faaliyetAciklama = faaliyetAciklama;
	}
	public String getSurezaman() {
		return surezaman;
	}
	public void setSurezaman(String surezaman) {
		this.surezaman = surezaman;
	}
	public String getSurezamanTuri() {
		return surezamanTuri;
	}
	public void setSurezamanTuri(String surezamanTuri) {
		this.surezamanTuri = surezamanTuri;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getBasangicTarihi() {
		return basangicTarihi;
	}
	public void setBasangicTarihi(Date basangicTarihi) {
		this.basangicTarihi = basangicTarihi;
	}
	public Date getBitisTarihi() {
		return bitisTarihi;
	}
	public void setBitisTarihi(Date bitisTarihi) {
		this.bitisTarihi = bitisTarihi;
	}
	public PerformansHedefi getPerformansHedefi() {
		return performansHedefi;
	}
	public void setPerformansHedefi(PerformansHedefi performansHedefi) {
		this.performansHedefi = performansHedefi;
	}
	
	
	
}
