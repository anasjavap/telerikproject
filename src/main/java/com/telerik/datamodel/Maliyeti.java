package com.telerik.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Maliyeti {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long malId;
	
	@ManyToOne
	private Faaliyet faaliyet;
	
	private Double yil;
	private Double ek1;
	private Double ek2;
	private Double ek3;
	private Double ek4;
	private Double ek5;
	private Double ek6;
	private Double ek7;
	private Double ek8;
	private Double ek9;
	
	
	private Double t1;
	private Double t2;
	private Double t3;
	private Double t4;
	
	public Long getMalId() {
		return malId;
	}
	public void setMalId(Long malId) {
		this.malId = malId;
	}
	public Double getYil() {
		return yil;
	}
	public void setYil(Double yil) {
		this.yil = yil;
	}
	public Double getEk1() {
		return ek1;
	}
	public void setEk1(Double ek1) {
		this.ek1 = ek1;
	}
	public Double getEk2() {
		return ek2;
	}
	public void setEk2(Double ek2) {
		this.ek2 = ek2;
	}
	public Double getEk3() {
		return ek3;
	}
	public void setEk3(Double ek3) {
		this.ek3 = ek3;
	}
	public Double getEk4() {
		return ek4;
	}
	public void setEk4(Double ek4) {
		this.ek4 = ek4;
	}
	public Double getEk5() {
		return ek5;
	}
	public void setEk5(Double ek5) {
		this.ek5 = ek5;
	}
	public Double getEk6() {
		return ek6;
	}
	public void setEk6(Double ek6) {
		this.ek6 = ek6;
	}
	public Double getEk7() {
		return ek7;
	}
	public void setEk7(Double ek7) {
		this.ek7 = ek7;
	}
	public Double getEk8() {
		return ek8;
	}
	public void setEk8(Double ek8) {
		this.ek8 = ek8;
	}
	public Double getEk9() {
		return ek9;
	}
	public void setEk9(Double ek9) {
		this.ek9 = ek9;
	}
	public Double getT1() {
		return t1;
	}
	public void setT1(Double t1) {
		this.t1 = t1;
	}
	public Double getT2() {
		return t2;
	}
	public void setT2(Double t2) {
		this.t2 = t2;
	}
	public Double getT3() {
		return t3;
	}
	public void setT3(Double t3) {
		this.t3 = t3;
	}
	public Double getT4() {
		return t4;
	}
	public void setT4(Double t4) {
		this.t4 = t4;
	}
	

}
