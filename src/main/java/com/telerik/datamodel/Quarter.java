package com.telerik.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Quarter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public int realValue;
	
	public int targetValue;

	public int sum;
	
	public int percentage;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getRealValue() {
		return realValue;
	}

	public void setRealValue(int realValue) {
		this.realValue = realValue;
	}

	public int getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(int targetValue) {
		this.targetValue = targetValue;
	}

	public int getSum() {
		return sum;
	}

	public void setSum() {
		this.sum = realValue + targetValue;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage() {
		this.percentage = (realValue*100) / targetValue;
	}
	
	
}
