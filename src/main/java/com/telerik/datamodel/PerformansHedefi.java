package com.telerik.datamodel;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PerformansHedefi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long perId;
	
	private String adi;
	
	private String aciklama;
	
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	@ManyToOne
	private StrategyPlanHedefler hedef;
	
	public Long getPerId() {
		return perId;
	}

	public void setPerId(Long perId) {
		this.perId = perId;
	}

	public String getAdi() {
		return adi;
	}

	public void setAdi(String adi) {
		this.adi = adi;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public StrategyPlanHedefler getHedef() {
		return hedef;
	}

	public void setHedef(StrategyPlanHedefler hedef) {
		this.hedef = hedef;
	}
	
	
	
}
