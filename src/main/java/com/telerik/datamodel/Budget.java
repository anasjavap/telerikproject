package com.telerik.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Budget {

	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long id;
	
	private int yil;
	
	private String aciklamaler;
	
	private double personalGider;
	private double sgk;
	private double malVeHizmat;
	private double faiz;
	private double cari;
	private double sermaye;
	private double sermayeTransfer;
	private double borc;
	
	private double donerSermaye;
	private double digerYurt;
	private double yurt;
	
	private double toplamButce;
	
	private double toplamDisi;
	
	private double toplamKaynek;
	
	public String getAceklamaler() {
		return aciklamaler;
	}

	public void setAceklamaler(String aciklamaler) {
		this.aciklamaler = aciklamaler;
	}

	public double getPersonalGider() {
		return personalGider;
	}

	public void setPersonalGider(double personalGider) {
		this.personalGider = personalGider;
	}

	public double getSgk() {
		return sgk;
	}

	public void setSgk(double sgk) {
		this.sgk = sgk;
	}

	public double getMalVeHizmat() {
		return malVeHizmat;
	}

	public void setMalVeHizmat(double malVeHizmat) {
		this.malVeHizmat = malVeHizmat;
	}

	public double getFaiz() {
		return faiz;
	}

	public void setFaiz(double faiz) {
		this.faiz = faiz;
	}

	public double getCari() {
		return cari;
	}

	public void setCari(double cari) {
		this.cari = cari;
	}

	public double getSermaye() {
		return sermaye;
	}

	public void setSermaye(double sermaye) {
		this.sermaye = sermaye;
	}

	public double getSermayeTransfer() {
		return sermayeTransfer;
	}

	public void setSermayeTransfer(double sermayeTransfer) {
		this.sermayeTransfer = sermayeTransfer;
	}

	public double getBorc() {
		return borc;
	}

	public void setBorc(double borc) {
		this.borc = borc;
	}

	public double getDonerSermaye() {
		return donerSermaye;
	}

	public void setDonerSermaye(double donerSermaye) {
		this.donerSermaye = donerSermaye;
	}

	public double getDigerYurt() {
		return digerYurt;
	}

	public void setDigerYurt(double digerYurt) {
		this.digerYurt = digerYurt;
	}

	public double getYurt() {
		return yurt;
	}

	public void setYurt(double yurt) {
		this.yurt = yurt;
	}

	public double getToplamButce() {
		return toplamButce;
	}

	public void setToplamButce(double toplamButce) {
		this.toplamButce = toplamButce;
	}

	public double getToplamDisi() {
		return toplamDisi;
	}

	public void setToplamDisi(double toplamDisi) {
		this.toplamDisi = toplamDisi;
	}

	public double getToplamKaynek() {
		return toplamKaynek;
	}

	public void setToplamKaynek(double toplamKaynek) {
		this.toplamKaynek = toplamKaynek;
	}

	@ManyToOne
	private Faaliyet faaliyet;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getYil() {
		return yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public Faaliyet getFaaliyet() {
		return faaliyet;
	}

	public void setFaaliyet(Faaliyet faaliyet) {
		this.faaliyet = faaliyet;
	}
	
	
	
}
