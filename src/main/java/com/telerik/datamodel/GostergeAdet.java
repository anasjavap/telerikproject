package com.telerik.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GostergeAdet {

	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long adetId;
	
	private String adi;
	
	private String islernTuru;

	public Long getAdetId() {
		return adetId;
	}

	public void setAdetId(Long adetId) {
		this.adetId = adetId;
	}

	public String getAdi() {
		return adi;
	}

	public void setAdi(String adi) {
		this.adi = adi;
	}

	public String getIslernTuru() {
		return islernTuru;
	}

	public void setIslernTuru(String islernTuru) {
		this.islernTuru = islernTuru;
	}
	
	
	
}
