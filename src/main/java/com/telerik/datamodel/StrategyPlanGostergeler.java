package com.telerik.datamodel;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class StrategyPlanGostergeler {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long gostergeId;
	private String adi;
	private int status;	
	@Temporal(TemporalType.DATE)
	private Date createDate;
	private int	OrderIndex;
	private String ackiama;
	
	
		
	@ManyToOne
	private PerformansHedefi performansHedefi;
	
	@OneToOne
	private GostergelerTur gostergelerTur;
	
	@OneToOne
	private GostergeOlcumSekil oclumsiklik;
	
	@OneToOne
	private GostergeAdet adetcCinsi;

		
	public Long getGostergeId() {
		return gostergeId;
	}
	public void setGostergeId(Long gostergeId) {
		this.gostergeId = gostergeId;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getOrderIndex() {
		return OrderIndex;
	}
	public void setOrderIndex(int orderIndex) {
		OrderIndex = orderIndex;
	}
	public String getAckiama() {
		return ackiama;
	}
	public void setAckiama(String ackiama) {
		this.ackiama = ackiama;
	}
	public PerformansHedefi getPerformansHedefi() {
		return performansHedefi;
	}
	public void setPerformansHedefi(PerformansHedefi performansHedefi) {
		this.performansHedefi = performansHedefi;
	}
	public GostergelerTur getGostergelerTur() {
		return gostergelerTur;
	}
	public void setGostergelerTur(GostergelerTur gostergelerTur) {
		this.gostergelerTur = gostergelerTur;
	}
	public GostergeOlcumSekil getOclumsiklik() {
		return oclumsiklik;
	}
	public void setOclumsiklik(GostergeOlcumSekil oclumsiklik) {
		this.oclumsiklik = oclumsiklik;
	}
	public GostergeAdet getAdetcCinsi() {
		return adetcCinsi;
	}
	public void setAdetcCinsi(GostergeAdet adetcCinsi) {
		this.adetcCinsi = adetcCinsi;
	}
	
	
}
