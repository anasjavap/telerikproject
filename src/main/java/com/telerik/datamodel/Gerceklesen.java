package com.telerik.datamodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Gerceklesen {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private StrategyPlanGostergeler gostergeler;

	@OneToOne(cascade =CascadeType.ALL)
	private Quarter quarter1 ;
	@OneToOne(cascade =CascadeType.ALL)
	private Quarter quarter2;
	@OneToOne(cascade =CascadeType.ALL)
	private Quarter quarter3;
	@OneToOne(cascade =CascadeType.ALL)
	private Quarter quarter4;
	
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	private String toplam;
	
	private int yil;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StrategyPlanGostergeler getGostergeler() {
		return gostergeler;
	}

	public void setGostergeler(StrategyPlanGostergeler gostergeler) {
		this.gostergeler = gostergeler;
	}

	public Quarter getQuarter1() {
		return quarter1;
	}

	public void setQuarter1(Quarter quarter1) {
		this.quarter1 = quarter1;
	}

	public Quarter getQuarter2() {
		return quarter2;
	}

	public void setQuarter2(Quarter quarter2) {
		this.quarter2 = quarter2;
	}

	public Quarter getQuarter3() {
		return quarter3;
	}

	public void setQuarter3(Quarter quarter3) {
		this.quarter3 = quarter3;
	}

	public Quarter getQuarter4() {
		return quarter4;
	}

	public void setQuarter4(Quarter quarter4) {
		this.quarter4 = quarter4;
	}

	public String getToplam() {
		return toplam;
	}

	public void setToplam(String toplam) {
		this.toplam = toplam;
	}

	public int getYil() {
		return yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getToplarn() {
		return toplam;
	}

	public void setToplarn(String toplarn) {
		this.toplam = toplarn;
	}
	
}
