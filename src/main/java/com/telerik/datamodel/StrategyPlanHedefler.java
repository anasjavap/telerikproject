package com.telerik.datamodel;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class StrategyPlanHedefler {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long hedefId;
	
	private String hedef;
	private int status;
	
	@Temporal(TemporalType.DATE)
	private Date createDate;
	private int	OrderIndex;
	private String ackiama;
	
	@ManyToOne
	private StrategyPlanAmaclar amaclar;

	
	public Long getHedefId() {
		return hedefId;
	}
	public void setHedefId(Long hedefId) {
		this.hedefId = hedefId;
	}
	public String getHedef() {
		return hedef;
	}
	public void setHedef(String hedef) {
		this.hedef = hedef;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getOrderIndex() {
		return OrderIndex;
	}
	public void setOrderIndex(int orderIndex) {
		OrderIndex = orderIndex;
	}
	public String getAckiama() {
		return ackiama;
	}
	public void setAckiama(String ackiama) {
		this.ackiama = ackiama;
	}

	public StrategyPlanAmaclar getAmaclar() {
		return amaclar;
	}
	public void setAmaclar(StrategyPlanAmaclar amaclar) {
		this.amaclar = amaclar;
	}
	
	

}
