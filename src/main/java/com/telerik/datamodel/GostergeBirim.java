package com.telerik.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class GostergeBirim {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long gbId;
	
	@ManyToOne
	private StrategyPlanGostergeler  gostergeler;
	
	private String girdi;
	private String cikti;
	private String sonuc;
	
	private String verimlilik;
	private String etkinlik;
	
	private String kalite;

	public Long getGbId() {
		return gbId;
	}

	public void setGbId(Long gbId) {
		this.gbId = gbId;
	}

	public StrategyPlanGostergeler getGostergeler() {
		return gostergeler;
	}

	public void setGostergeler(StrategyPlanGostergeler gostergeler) {
		this.gostergeler = gostergeler;
	}

	public String getGirdi() {
		return girdi;
	}

	public void setGirdi(String girdi) {
		this.girdi = girdi;
	}

	public String getCikti() {
		return cikti;
	}

	public void setCikti(String cikti) {
		this.cikti = cikti;
	}

	public String getSonuc() {
		return sonuc;
	}

	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}

	public String getVerimlilik() {
		return verimlilik;
	}

	public void setVerimlilik(String verimlilik) {
		this.verimlilik = verimlilik;
	}

	public String getEtkinlik() {
		return etkinlik;
	}

	public void setEtkinlik(String etkinlik) {
		this.etkinlik = etkinlik;
	}

	public String getKalite() {
		return kalite;
	}

	public void setKalite(String kalite) {
		this.kalite = kalite;
	}
	
}
