<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@tag pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link href="<c:url value='/resources/styles/kendo.common-material.min.css'/>" rel="stylesheet" />
<link href="<c:url value='/resources/styles/kendo.common-material.core.min.css'/>" rel="stylesheet" />
<link href="<c:url value='/resources/styles/kendo.common.min.css'/>" rel="stylesheet" />

<link href="<c:url value='/resources/styles/kendo.material.min.css'/>" rel="stylesheet" />
<link href="<c:url value='/resources/styles/kendo.dataviz.min.css'/>" rel="stylesheet" />
<link href="<c:url value='/resources/styles/kendo.dataviz.material.min.css'/>" rel="stylesheet" />

<link href="<c:url value='/resources/styles/styles.css'/>" rel="stylesheet" />

<link rel="stylesheet"
	href="<c:url value='https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en'/>" />
<link rel="stylesheet" href="<c:url value='https://fonts.googleapis.com/icon?family=Material+Icons'/>" />
<link rel="stylesheet" href="<c:url value='https://code.getmdl.io/1.1.3/material.deep_purple-pink.min.css'/>" />

<script src="<c:url value='/resources/js/jquery.min.js' />"></script>
<script src="<c:url value='/resources/js/custom.js' />"></script>
<script src="<c:url value='/resources/js/kendo.all.min.js' />"></script>
<script src="<c:url value='/resources/js/kendo.timezones.min.js' />"></script>
<script src="<c:url value='/resources/js/jszip.min.js' />"></script>
<script src="<c:url value='/resources/js/messages/kendo.messages.tr-TR.min.js'/>"></script>


</head>
<body class="mdl-demo mdl-color-text--grey-700 mdl-base backBody">
	<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
		<header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
		 <div style="height: 50px;background-color: #2EA3F2;">
		 </div>
		<div class="mdl-layout--large-screen-only mdl-layout__header-row" style="background-color:white;height: 80px">
			 <div>
				<img src="${pageContext.request.contextPath}/resources/icon/kiosgrc.jpg" width="200px" height="50px" />
			</div>
			
			<div style="padding-left:27%;">
			 <ul class="ul-menu">
 					 <li class="li-menu"><a href="${pageContext.request.contextPath}/index/" >Planlar</a></li>
					
					 <li class="li-menu"><a href="${pageContext.request.contextPath}/user/" >Kullanıcı Yönetimi</a> </li>

 					 <li class="li-menu"><a href="${pageContext.request.contextPath}/setting/">Ayarlar</a></li>	
 					 
 					 <li class="li-menu"><a href="${pageContext.request.contextPath}/gosterHome/">Gostergeler</a></li>	
 					  
 					  <li class="li-menu"><a href="${pageContext.request.contextPath}/faaliyetHome/">Faaliyet</a></li>	
				</ul> 
			
			</div>
			<div style="color : #2EA3F2;">
				<a href="${pageContext.request.contextPath}/logout" class="btn btn-danger btn-sm"> <img
					src="${pageContext.request.contextPath}/resources/icon/logout-512.png" width="22px" height="25px" />
				</a>&nbsp;&nbsp;&nbsp;
				<sec:authentication property="principal.username"/>
			</div>
			
			<div style="padding-left: 3%;">
		    	<img src="${pageContext.request.contextPath}/resources/icon/KOSGEB.jpg" width="150px" height="70px" />
			</div>
			
		</div>
		
		<div class="mdl-layout--large-screen-only mdl-layout__header-row">
		</div>
		
		</header>
	</div>
	<div class="page" style="background-color: white;overflow: auto">