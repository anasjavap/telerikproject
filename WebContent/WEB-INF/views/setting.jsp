<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="temp" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>


<temp:header />


<kendo:tabStrip name="tabStrip">
	<kendo:tabStrip-animation>
		<kendo:tabStrip-animation-open effects="fadeIn"></kendo:tabStrip-animation-open>
	</kendo:tabStrip-animation>
	<kendo:tabStrip-items>
	
		<kendo:tabStrip-item text="Tur" selected="true">
			<kendo:tabStrip-item-content>
				<jsp:include page="tur.jsp" />
			</kendo:tabStrip-item-content>
		</kendo:tabStrip-item>
		
		<kendo:tabStrip-item text="Olcum" >
			<kendo:tabStrip-item-content>
				<jsp:include page="olcumsikel.jsp" />
			</kendo:tabStrip-item-content>
		</kendo:tabStrip-item>
		
		<kendo:tabStrip-item text="Adet" >
			<kendo:tabStrip-item-content>
				<jsp:include page="adet.jsp" />
			</kendo:tabStrip-item-content>
		</kendo:tabStrip-item>

	</kendo:tabStrip-items>
</kendo:tabStrip>
<temp:footer />