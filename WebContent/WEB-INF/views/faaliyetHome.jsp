<%@page import="com.telerik.datamodel.StrategyPlanAmaclar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="temp" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%
	HashMap<String, Object> dataDept = new HashMap<String, Object>();
	dataDept.put("deptId", "#=deptId#");
%>

<c:url value="/dept/read" var="deptReadUrl" />

<c:url value="/faaliyet/readForHome?id=" var="faaliyetReadUrl" />
<c:url value="/faaliyet/update" var="faaliyetUpdateUrl" />
<c:url value="/faaliyet/delete" var="faaliyetDeleteUrl" />

<temp:header />

<div style="float: left; width: 20%; padding: 10px 10px; background-color: white;">
	<kendo:grid name="grid_dept" sortable="true" filterable="true" selectable="true" change="onChange_faaliyet">

		<kendo:grid-columns>
			<kendo:grid-column title="Name" field="name" />
		</kendo:grid-columns>

		<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
			<kendo:dataSource-transport>
				<kendo:dataSource-transport-read url="${deptReadUrl}" type="POST" dataType="json" contentType="application/json" />
				<kendo:dataSource-transport-parameterMap>
					<script>
						function parameterMap(options) {
							return JSON.stringify(options);
						}
					</script>
				</kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport>
			<kendo:dataSource-schema>
				<kendo:dataSource-schema-model id="deptId">
					<kendo:dataSource-schema-model-fields>
						<kendo:dataSource-schema-model-field name="name" type="string" />
					</kendo:dataSource-schema-model-fields>
				</kendo:dataSource-schema-model>
			</kendo:dataSource-schema>
		</kendo:dataSource>
	</kendo:grid>

</div>

<div class="layoutpagecontent">
	<kendo:grid name="grid_faaliyet" sortable="true" filterable="true" selectable="true" detailTemplate="faaliyet_temp">
		<kendo:grid-editable mode="popup" />
	    <kendo:grid-filterable mode="row"/>
	 
		<kendo:grid-columns>
			<kendo:grid-column title="Adi" field="faaliyetAdi">
				<kendo:grid-column-filterable>
					<kendo:grid-column-filterable-cell operator="contains" />
				</kendo:grid-column-filterable>
			</kendo:grid-column>
			<kendo:grid-column title="Status" field="status" filterable="false" />
			<kendo:grid-column title="Aciklama" field="faaliyetAciklama" filterable="false"/>
			<kendo:grid-column title="Sure Zaman" field="surezaman" filterable="false" />
			<kendo:grid-column title="Sure Zaman Turi" field="surezamanTuri" filterable="false" />
			<kendo:grid-column title="Created Date" field="createdDate" format="{0: dd-MMM-yyyy}" filterable="false" hidden="true"/>
			<kendo:grid-column title="Basangic Tarihi" field="basangicTarihi" format="{0: dd-MMM-yyyy}" filterable="false" />
			<kendo:grid-column title="Bitis Tarihi" field="bitisTarihi" format="{0: dd-MMM-yyyy}" filterable="false"/>
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text="" />
					<kendo:grid-column-commandItem name="destroy" text="" />
				</kendo:grid-column-command>
			</kendo:grid-column>
		</kendo:grid-columns>
		<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
			<kendo:dataSource-transport>
				<kendo:dataSource-transport-read url="${faaliyetReadUrl}" data="<%=dataDept%>" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-update url="${faaliyetUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-destroy url="${faaliyetDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-parameterMap>
					<script>
						function parameterMap(options) {
							return JSON.stringify(options);
						}
					</script>
				</kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport>


			<kendo:dataSource-schema>
				<kendo:dataSource-schema-model id="faaliyetId">
					<kendo:dataSource-schema-model-fields>
						<kendo:dataSource-schema-model-field name="faaliyetAdi" type="string" />
						<kendo:dataSource-schema-model-field name="status" type="number" />
						<kendo:dataSource-schema-model-field name="faaliyetAciklama" type="string" />
						<kendo:dataSource-schema-model-field name="surezaman" type="string" />
						<kendo:dataSource-schema-model-field name="surezamanTuri" type="string" />
						<kendo:dataSource-schema-model-field name="createdDate" type="date" />
						<kendo:dataSource-schema-model-field name="basangicTarihi" type="date" />
						<kendo:dataSource-schema-model-field name="bitisTarihi" type="date" />
					</kendo:dataSource-schema-model-fields>
				</kendo:dataSource-schema-model>
			</kendo:dataSource-schema>
		</kendo:dataSource>
	</kendo:grid>


<kendo:grid-detailTemplate id="faaliyet_temp">
	<jsp:include page="budget.jsp">
		<jsp:param value="#=faaliyetId#" name="faaliyetId" />
	</jsp:include>
</kendo:grid-detailTemplate>

</div>

<script type="text/javascript">
function onChange_faaliyet(e) {
	grid = e.sender;
	var currentDataItem = grid.dataItem(this.select());
	var grid = $("#grid_faaliyet").data("kendoGrid");
	grid.dataSource.options.transport.read.url = "${pageContext.request.contextPath}/faaliyet/readForHome?id="
			+ currentDataItem.id;
	grid.dataSource.read();
	grid.refresh();
}
</script>
<temp:footer />