<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page import="java.util.HashMap"%>

<c:url value="/performans/read" var="performansReadUrl" />
<c:url value="/performans/update" var="performansUpdateUrl" />
<c:url value="/performans/delete" var="performansDeleteUrl" />
<c:url value="/performans/create" var="performansCreateUrl" />

<%
	HashMap<String, Object> hedefData = new HashMap<String, Object>();
	hedefData.put("hedefId", request.getParameter("hedef1"));
%>

<kendo:grid name="grid_perf_${param.hedef1}" sortable="true" filterable="true" selectable="true">

	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />
		<kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" text=""/>
		</kendo:grid-toolbar>
	</sec:authorize>
	<kendo:grid-columns>

		<kendo:grid-column title="Created Date" field="createdDate" format="{0: dd-MMM-yyyy}" hidden="true"/>
		<kendo:grid-column title="Status" field="status" hidden="true"/>
		<kendo:grid-column title="Order Index" field="orederIndex" hidden="true"/>
		<kendo:grid-column title="Mevcutdurum" field="mevcutdurum" hidden="true"/>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text="" />
					<kendo:grid-column-commandItem name="destroy"  text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
		</sec:authorize>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${performansReadUrl}" data="<%=hedefData %>" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${performansUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${performansDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${performansCreateUrl}" data="<%=hedefData %>" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>


		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="perId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="createdDate" type="date" />
					<kendo:dataSource-schema-model-field name="status" type="number" />
					<kendo:dataSource-schema-model-field name="orederIndex" type="number" />
					<kendo:dataSource-schema-model-field name="mevcutdurum" type="string" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>
