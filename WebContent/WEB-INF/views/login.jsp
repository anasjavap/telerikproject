<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<head>
<title>Home</title>

<link href="<c:url value='/resources/styles/styles.css'/>" rel="stylesheet" />

<link rel="stylesheet" href="<c:url value='https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en'/>" />
<link rel="stylesheet" href="<c:url value='https://fonts.googleapis.com/icon?family=Material+Icons'/>" />
<link rel="stylesheet" href="<c:url value='https://code.getmdl.io/1.1.3/material.deep_purple-pink.min.css'/>" />

</head>
<body style="background-color: #2EA3F2;">
		<header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
		 <div style="height: 50px;background-color: #2EA3F2;">
		 </div>
		<div class="mdl-layout--large-screen-only mdl-layout__header-row" style="background-color:white;height: 80px">
			 <div>
				<img src="${pageContext.request.contextPath}/resources/icon/kiosgrc.jpg" width="200px" height="50px" />
			</div>
					
			<div style="padding-left: 70%;">
		    	<img src="${pageContext.request.contextPath}/resources/icon/KOSGEB.jpg" width="150px" height="70px" />
			</div>
				
		</header>
	</div>
	<div class="page">
		<form:form method="post" name="loginForm" action="login">
				<div class="logindiv">
					<c:if test="${param.error == 'true'}">
   						<div style="color: red">Kullanıcı adı veya şifre yanlış</div>
					</c:if>
				<input class="logininput" placeholder="Kullanıcı adı" name="user" type="text" required="Kullanıcı adı gerekli"> 
				<input class="logininput" placeholder="Şifre" name="password" type="password" required="Şifre gerekli">
				<button class="loginbutton">Giriş Yap</button>
				<div style="color: red">${error}</div>
			</div>
		</form:form>
	</div>
</body>