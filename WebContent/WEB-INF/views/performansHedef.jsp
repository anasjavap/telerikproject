<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%
	HashMap<String, Object> hedefData = new HashMap<String, Object>();
	hedefData.put("hedefId", request.getParameter("hedef2"));
%>

<c:url value="/perfhedef/read" var="perfhedefReadUrl" />
<c:url value="/perfhedef/update" var="perfhedefUpdateUrl" />
<c:url value="/perfhedef/delete" var="perfhedefDeleteUrl" />
<c:url value="/perfhedef/create" var="perfhedefCreateUrl" />


<kendo:grid name="grid_perfhedef_${param.hedef2}" sortable="true" detailTemplate="${param.temp_perf}">
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />
		<kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" text=""/>
		</kendo:grid-toolbar>
	</sec:authorize>
	<kendo:grid-columns>
		<kendo:grid-column title="Adi" field="adi" />
		<kendo:grid-column title="Aciklama" field="aciklama" hidden="true"/>
		<kendo:grid-column title="Created Date" field="createdDate" format="{0: dd-MMM-yyyy}" hidden="true"/>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text=""/>
					<kendo:grid-column-commandItem name="destroy" text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
		</sec:authorize>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${perfhedefReadUrl}" data="<%=hedefData %>" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${perfhedefUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${perfhedefDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${perfhedefCreateUrl}" data="<%=hedefData %>" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>

		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="perId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="createdDate" type="date" />
					<kendo:dataSource-schema-model-field name="adi" type="string" />
					<kendo:dataSource-schema-model-field name="aciklama" type="string" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>



