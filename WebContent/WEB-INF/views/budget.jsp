<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page import="java.util.HashMap"%>
<%
	HashMap<String, Object> faaliytData = new HashMap<String, Object>();
	faaliytData.put("faaliyetId", request.getParameter("faaliyetId"));
%>

<c:url value="/budget/read" var="budgetReadUrl" />
<c:url value="/budget/update" var="budgetUpdateUrl" />
<c:url value="/budget/delete" var="budgetDeleteUrl" />
<c:url value="/budget/create" var="budgetCreateUrl" />

<kendo:grid name="grid_budget_${param.faaliyetId}" sortable="true" filterable="true" selectable="true">
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />
		  <kendo:grid-filterable mode="row"/>
		<kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" text="" />
		</kendo:grid-toolbar>
	</sec:authorize>
	<kendo:grid-columns>
		<kendo:grid-column title="Yil" field="yil" >
		<kendo:grid-column-filterable>
					<kendo:grid-column-filterable-cell showOperators="false" />
		</kendo:grid-column-filterable>
		</kendo:grid-column>
		<kendo:grid-column title="ACik" field="aciklamaler" filterable="false" />
	
		<kendo:grid-column title="Personal " field="personalGider" hidden="true" filterable="false"/>
		<kendo:grid-column title="SGK" field="sgk"  hidden="true" />
		<kendo:grid-column title="MAL" field="malVeHizmat"  hidden="true" filterable="false" />
		<kendo:grid-column title="Faiz" field="faiz"  hidden="true" filterable="false"/>
		<kendo:grid-column title="Cari" field="cari"  hidden="true" filterable="false"/>
		<kendo:grid-column title="Sermaye" field="sermaye"  hidden="true" filterable="false"/>
		<kendo:grid-column title="Sermaye Transfer" field="sermayeTransfer"  hidden="true" filterable="false"/>
		<kendo:grid-column title="Borc" field="borc"  hidden="true" filterable="false"/>
	
		<kendo:grid-column title="Doner Sermaye" field="donerSermaye"  hidden="true" filterable="false"/>
		<kendo:grid-column title="Diger Yurt" field="digerYurt"  hidden="true" filterable="false"/>
		<kendo:grid-column title="Yurt" field="yurt"  hidden="true" filterable="false"/>
		
		
		<kendo:grid-column title="Toplam Butce" field="toplamButce"  filterable="false"/>
		<kendo:grid-column title="Toplam Disi" field="toplamDisi" filterable="false"/>
		<kendo:grid-column title="Toplam Kaynek" field="toplamKaynek" filterable="false"/>

		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text="" />
					<kendo:grid-column-commandItem name="destroy" text="" />
				</kendo:grid-column-command>
			</kendo:grid-column>
		</sec:authorize>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${budgetReadUrl}" data="<%=faaliytData %>" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${budgetUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${budgetDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${budgetCreateUrl}" data="<%=faaliytData %>" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>


		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="id">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="yil" type="number" />
					<kendo:dataSource-schema-model-field name="aciklamaler" type="string" />

					<kendo:dataSource-schema-model-field name="personalGider" type="number" />
					<kendo:dataSource-schema-model-field name="sgk" type="number" />
					<kendo:dataSource-schema-model-field name="malVeHizmat" type="number" />
					<kendo:dataSource-schema-model-field name="faiz" type="number" />
					<kendo:dataSource-schema-model-field name="cari" type="number" />
					<kendo:dataSource-schema-model-field name="sermaye" type="number" />
					<kendo:dataSource-schema-model-field name="sermayeTransfer" type="number" />
					<kendo:dataSource-schema-model-field name="borc" type="number" />


					<kendo:dataSource-schema-model-field name="donerSermaye" type="number" />
					<kendo:dataSource-schema-model-field name="digerYurt" type="number" />
					<kendo:dataSource-schema-model-field name="yurt" type="number" />


					<kendo:dataSource-schema-model-field name="toplamButce" type="number" editable="false" />
					<kendo:dataSource-schema-model-field name="toplamDisi" type="number" editable="false"/>
					<kendo:dataSource-schema-model-field name="toplamKaynek" type="number" editable="false"/>
					<kendo:dataSource-schema-model-field name="aciklamaler" type="string" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>
