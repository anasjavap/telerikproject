<%@page import="com.telerik.datamodel.StrategyPlanAmaclar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="temp" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%
	HashMap<String, Object> dataDept = new HashMap<String, Object>();
	dataDept.put("deptId", "#=deptId#");
	
	HashMap<String, Object> userData = new HashMap<String, Object>();
	userData.put("username", request.getUserPrincipal().getName() );
%>

<c:url value="/dept/read" var="deptReadUrl" />
<c:url value="/dept/update" var="deptUpdateUrl" />
<c:url value="/dept/delete" var="deptDeleteUrl" />
<c:url value="/dept/create" var="deptCreateUrl" />


<temp:header />

<div style="float: left; width: 20%; padding: 10px 10px;background-color: white;">
	<kendo:grid name="grid_dept" sortable="true" filterable="true" selectable="true" change="onChange">
		<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />		
		<kendo:grid-toolbar >
			<kendo:grid-toolbarItem name="create" text=""/>
			<kendo:grid-toolbarItem name="excel" text=""/>
		</kendo:grid-toolbar>
		</sec:authorize>
		<kendo:grid-excel fileName="dept.xlsx" filterable="true" proxyURL="${saveUrl}" />
		<kendo:grid-columns>
			<kendo:grid-column title="Name" field="name" />		
			<sec:authorize access="hasRole('ROLE_ADMIN')">	
			<kendo:grid-column title="&nbsp;" width="75px" >
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text=""/>
					<kendo:grid-column-commandItem name="destroy" text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
			</sec:authorize>
		</kendo:grid-columns>
		<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
			<kendo:dataSource-transport>
				<kendo:dataSource-transport-read url="${deptReadUrl}" type="POST" dataType="json" contentType="application/json" />
				<kendo:dataSource-transport-update url="${deptUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-destroy url="${deptDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-create url="${deptCreateUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-parameterMap>
					<script>
						function parameterMap(options) {
							return JSON.stringify(options);
						}
					</script>
				</kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport>
			<kendo:dataSource-schema>
				<kendo:dataSource-schema-model id="deptId">
					<kendo:dataSource-schema-model-fields>
						<kendo:dataSource-schema-model-field name="name" type="string" />
					</kendo:dataSource-schema-model-fields>
				</kendo:dataSource-schema-model>
			</kendo:dataSource-schema>
		</kendo:dataSource>
	</kendo:grid>

</div>

<div class="layoutpagecontent">
	<jsp:include page="amac.jsp">
	<jsp:param value="<%=dataDept%>" name="dataDept"/>
	</jsp:include>
</div>


<script type="text/javascript">
	function onChange(e) {
		grid = e.sender;
		var currentDataItem = grid.dataItem(this.select());
		var grid = $("#grid_amac").data("kendoGrid");
		grid.dataSource.options.transport.read.url = "${pageContext.request.contextPath}/amac/read?id="+ currentDataItem.id;
		grid.dataSource.read();
		grid.refresh();
	}
	
	$("#grid .k-grid-content").css({
	    "overflow-y": "hidden"
	});
</script>
<temp:footer />