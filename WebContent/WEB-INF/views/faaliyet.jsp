<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%
	HashMap<String, Object> perforHedData = new HashMap<String, Object>();
	perforHedData.put("perfId", request.getParameter("perf2"));
%>



<c:url value="/faaliyet/read" var="faaliyetReadUrl" />
<c:url value="/faaliyet/update" var="faaliyetUpdateUrl" />
<c:url value="/faaliyet/delete" var="faaliyetDeleteUrl" />
<c:url value="/faaliyet/create" var="faaliyetCreateUrl" />


<kendo:grid name="grid_faaliyet_${param.perf2}" sortable="true" filterable="true" selectable="true" detailTemplate="${param.faaliyet_temp}">
	<kendo:grid-editable mode="popup" />
	<kendo:grid-toolbar>
		<kendo:grid-toolbarItem name="create" text=""/>
	</kendo:grid-toolbar>
	<kendo:grid-columns>
		<kendo:grid-column title="Adi" field="faaliyetAdi" />
		<kendo:grid-column title="Status" field="status" />
		<kendo:grid-column title="Aciklama" field="faaliyetAciklama" />
		<kendo:grid-column title="Sure Zaman" field="surezaman" />
		<kendo:grid-column title="Sure Zaman Turi" field="surezamanTuri" />
		<kendo:grid-column title="Created Date" field="createdDate"  format="{0: dd-MMM-yyyy}" />
		<kendo:grid-column title="Basangic Tarihi" field="basangicTarihi"   format="{0: dd-MMM-yyyy}"/>
		<kendo:grid-column title="Bitis Tarihi" field="bitisTarihi"  format="{0: dd-MMM-yyyy}"/>
		<kendo:grid-column title="&nbsp;" width="75px">
			<kendo:grid-column-command>
				<kendo:grid-column-commandItem name="edit" text=""/>
				<kendo:grid-column-commandItem name="destroy" text=""/>
			</kendo:grid-column-command>
		</kendo:grid-column>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${faaliyetReadUrl}" data="<%=perforHedData%>" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${faaliyetUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${faaliyetDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${faaliyetCreateUrl}" data="<%=perforHedData%>" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>


		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="faaliyetId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="faaliyetAdi" type="string" />
					<kendo:dataSource-schema-model-field name="status" type="number" />
					<kendo:dataSource-schema-model-field name="faaliyetAciklama" type="string" />
					<kendo:dataSource-schema-model-field name="surezaman" type="string" />
					<kendo:dataSource-schema-model-field name="surezamanTuri" type="string" />
					<kendo:dataSource-schema-model-field name="createdDate" type="date" />
					<kendo:dataSource-schema-model-field name="basangicTarihi" type="date" />
					<kendo:dataSource-schema-model-field name="bitisTarihi" type="date" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>
