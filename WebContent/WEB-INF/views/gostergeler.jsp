<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%
	HashMap<String, Object> perforHedefData = new HashMap<String, Object>();
    perforHedefData.put("perfId", request.getParameter("perf1"));
%>

<c:url value="/gosterge/read" var="gostergeReadUrl" />
<c:url value="/gosterge/update" var="gostergeUpdateUrl" />
<c:url value="/gosterge/delete" var="gostergeDeleteUrl" />
<c:url value="/gosterge/create" var="gostergeCreateUrl" />

<c:url value="/tur/read" var="turUrl" />
<c:url value="/adet/read" var="adetUrl" />
<c:url value="/olcum/read" var="olcumUrl" />


<kendo:grid name="grid_gos_${param.perf1}"  pageable="true" sortable="true" scrollable="false" detailTemplate="${param.goster_temp}">

	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />
		<kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" text="" />
		</kendo:grid-toolbar>
	</sec:authorize>
	<kendo:grid-columns>
		<kendo:grid-column title="Adi" field="adi" />
		<kendo:grid-column title="Aciklama" field="ackiama" />
		<kendo:grid-column title="Status" field="status" hidden="true" />
		<kendo:grid-column title="Create Date" field="createDate" format="{0: dd-MMM-yyyy}" hidden="true" />
		<kendo:grid-column title="OrderIndex" field="orderIndex" hidden="true" />
		<kendo:grid-column title="Tur" field="gostergelerTur" editor="${param.nameTurEditor}" template="\\#=gostergelerTur.adi\\#" />
		<kendo:grid-column title="Adet" field="adetcCinsi" editor="${param.nameAdetEditor}" template="\\#=oclumsiklik.olcumSekil\\#"/>
		<kendo:grid-column title="Oclum" field="oclumsiklik" editor="${param.nameOlcumEditor}" template="\\#=adetcCinsi.adi\\#"/>


		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text=""/>
					<kendo:grid-column-commandItem name="destroy"  text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
		</sec:authorize>
		
	</kendo:grid-columns>
	<kendo:dataSource pageSize="10" serverPaging="true" serverSorting="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${gostergeReadUrl}" data="<%=perforHedefData%>" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${gostergeUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${gostergeDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${gostergeCreateUrl}" data="<%=perforHedefData%>" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>

		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="gostergeId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="adi" type="string" />
					<kendo:dataSource-schema-model-field name="ackiama" type="string" />
					<kendo:dataSource-schema-model-field name="status" type="number" />
					<kendo:dataSource-schema-model-field name="createDate" type="date" />
					<kendo:dataSource-schema-model-field name="orderIndex" type="number" />
					<kendo:dataSource-schema-model-field name="gostergelerTur" />
					<kendo:dataSource-schema-model-field name="oclumsiklik" />
					<kendo:dataSource-schema-model-field name="adetcCinsi" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>
