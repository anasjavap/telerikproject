<%@page import="com.telerik.datamodel.Gerceklesen"%>
<%@page import="com.telerik.datamodel.Quarter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%
	HashMap<String, Object> gercekData = new HashMap<String, Object>();
	gercekData.put("gostergeId", request.getParameter("gostergeId"));

%>

<c:url value="/gerceklesen/read" var="gerceklesenReadUrl" />
<c:url value="/gerceklesen/update" var="gerceklesenUpdateUrl" />
<c:url value="/gerceklesen/delete" var="gerceklesenDeleteUrl" />
<c:url value="/gerceklesen/create" var="gerceklesenCreateUrl" />


<kendo:grid name="grid_ger_${param.gostergeId}"  sortable="true" >
	<kendo:grid-editable mode="inline"  />
	<kendo:grid-toolbar>
		<kendo:grid-toolbarItem name="create" text="" />
	</kendo:grid-toolbar>
	<kendo:grid-columns>

		<kendo:grid-column title="Yil" field="yil" attributes=" style='color:red;'"/>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-column title="T1" field="quarter1" editor="${param.editorquart1Admin}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter1.realValue\\#</div><div  style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter1.targetValue\\#</div> "/>
		<kendo:grid-column title="T2" field="quarter2" editor="${param.editorquart2Admin}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter2.realValue\\# </div><div style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter2.targetValue\\# </div> " />
		<kendo:grid-column title="T3" field="quarter3" editor="${param.editorquart3Admin}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter3.realValue\\# </div><div style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter3.targetValue\\# </div> "/>
		<kendo:grid-column title="T4" field="quarter4" editor="${param.editorquart4Admin}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter4.realValue\\# </div><div style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter4.targetValue\\# </div> "/>
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_USER')">
		<kendo:grid-column title="T1" field="quarter1" editor="${param.editorquart1User}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter1.realValue\\#</div><div  style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter1.targetValue\\#</div> "/>
		<kendo:grid-column title="T2" field="quarter2" editor="${param.editorquart2User}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter2.realValue\\# </div><div style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter2.targetValue\\# </div> " />
		<kendo:grid-column title="T3" field="quarter3" editor="${param.editorquart3User}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter3.realValue\\# </div><div style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter3.targetValue\\# </div> "/>
		<kendo:grid-column title="T4" field="quarter4" editor="${param.editorquart4User}" attributes=" style='padding: 0px;text-align: center'" template="<div style='width=100%;height=20px;background-color: rgb(235, 248, 34);'>\\#=quarter4.realValue\\# </div><div style='width=100%;height=20px;background-color: rgb(235, 200, 34);'>\\#=quarter4.targetValue\\# </div> "/>
		</sec:authorize>
		<kendo:grid-column title="Created Date" field="createdDate" format="{0: dd-MMM-yyyy}" />
		<kendo:grid-column title="Toplam" field="toplam" />
		
		<kendo:grid-column  title="&nbsp;" width="75px" >
			<kendo:grid-column-command>
				<kendo:grid-column-commandItem name="edit" text=""/>
				<kendo:grid-column-commandItem name="destroy" text=""/>
			</kendo:grid-column-command>
		</kendo:grid-column>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${gerceklesenReadUrl}" data="<%=gercekData%>"  type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${gerceklesenUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${gerceklesenDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${gerceklesenCreateUrl}" data="<%=gercekData%>" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
                			return JSON.stringify(options);
                		
               		}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>

		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="id">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="createdDate" type="date" />
					<kendo:dataSource-schema-model-field name="quarter1" defaultValue="<%= new Quarter() %>"/>
					<kendo:dataSource-schema-model-field name="quarter2" defaultValue="<%= new Quarter() %>" />
					<kendo:dataSource-schema-model-field name="quarter3" defaultValue="<%= new Quarter() %>"/>
					<kendo:dataSource-schema-model-field name="quarter4" defaultValue="<%= new Quarter() %>"/>
					<kendo:dataSource-schema-model-field name="toplam" />
					<kendo:dataSource-schema-model-field name="yil" type="number" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>

