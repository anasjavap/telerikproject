<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>



<c:url value="/adet/readall" var="adetReadUrl" />
<c:url value="/adet/update" var="adetUpdateUrl" />
<c:url value="/adet/delete" var="adetDeleteUrl" />
<c:url value="/adet/create" var="adetCreateUrl" />

<kendo:grid name="grid_adet" sortable="true" filterable="true" selectable="true">
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />
		<kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" />
		</kendo:grid-toolbar>
	</sec:authorize>
	<kendo:grid-columns>
		<kendo:grid-column title="Adi" field="adi" />
		<kendo:grid-column title="Islern Turu" field="islernTuru" />
		<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-column title="&nbsp;" width="75px">
			<kendo:grid-column-command>
				<kendo:grid-column-commandItem name="edit" text=""/>
				<kendo:grid-column-commandItem name="destroy"  text=""/>
			</kendo:grid-column-command>
		</kendo:grid-column>
		</sec:authorize>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${adetReadUrl}" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${adetUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${adetDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${adetCreateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>


		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="adetId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="adi" type="string" />
					<kendo:dataSource-schema-model-field name="islernTuru" type="string" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>
