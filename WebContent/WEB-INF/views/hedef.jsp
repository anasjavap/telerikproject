<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%
	HashMap<String, Object> amacData = new HashMap<String, Object>();
	amacData.put("amacId", request.getParameter("amacId"));
%>

<c:url value="/hedef/read" var="hedefReadUrl" />
<c:url value="/hedef/update" var="hedefUpdateUrl" />
<c:url value="/hedef/delete" var="hedefDeleteUrl" />
<c:url value="/hedef/create" var="hedefCreateUrl" />

<kendo:grid name="grid_hedef_${param.amacId}" pageable="true" sortable="true" scrollable="false" detailTemplate="template_hedef">
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />
		<kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" text=""/>
			<kendo:grid-toolbarItem name="excel" text=""/>
		</kendo:grid-toolbar>
	</sec:authorize>
	<kendo:grid-excel fileName="hedef.xlsx" filterable="true" proxyURL="${saveUrl}" />

	<kendo:grid-columns>
		<kendo:grid-column title="Hedef" field="hedef" />
		<kendo:grid-column title="Aciklama" field="ackiama" hidden="true" />
		<kendo:grid-column title="Status" field="status" hidden="true" />
		<kendo:grid-column title="Create Date" field="createDate" format="{0: dd-MMM-yyyy}" hidden="true" />
		<kendo:grid-column title="OrderIndex" field="orderIndex" hidden="true" />
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text="" />
					<kendo:grid-column-commandItem name="destroy" text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
		</sec:authorize>
	</kendo:grid-columns>
	<kendo:dataSource pageSize="10" serverPaging="true" serverSorting="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${hedefReadUrl}" data="<%=amacData%>" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${hedefUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${hedefDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${hedefCreateUrl}" data="<%=amacData%>" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>

		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="hedefId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="hedef" type="string" />
					<kendo:dataSource-schema-model-field name="ackiama" type="string" />
					<kendo:dataSource-schema-model-field name="status" type="number" />
					<kendo:dataSource-schema-model-field name="createDate" type="date" />
					<kendo:dataSource-schema-model-field name="orderIndex" type="number" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>

