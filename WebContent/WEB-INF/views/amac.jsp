<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%
	HashMap<String, Object> amacData = new HashMap<String, Object>();
	amacData.put("amacId", "#=amacId#");

	HashMap<String, Object> hedefData = new HashMap<String, Object>();
	hedefData.put("hedefId", "#=hedefId#");

	HashMap<String, Object> perData = new HashMap<String, Object>();
	perData.put("perId", "#=perId#");

	HashMap<String, Object> gosData = new HashMap<String, Object>();
	gosData.put("gostergeId", "#=gostergeId#");
	
	HashMap<String, Object> gercekData = new HashMap<String, Object>();
	gercekData.put("gostergeId", "#=gostergeId#");
	
	HashMap<String, Object> faaliyetkData = new HashMap<String, Object>();
	faaliyetkData.put("faaliyetId", "#=faaliyetId#");

%>

<c:url value="/amac/read" var="amacReadUrl" />
<c:url value="/amac/update" var="amacUpdateUrl" />
<c:url value="/amac/delete" var="amacDeleteUrl" />
<c:url value="/amac/create" var="amacCreateUrl" />

<kendo:grid name="grid_amac" pageable="true" sortable="true" filterable="true" detailTemplate="template_amac">
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />
		<kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" text="" />
			<kendo:grid-toolbarItem name="excel"  text=""/>
		</kendo:grid-toolbar>
	</sec:authorize>
	<kendo:grid-excel fileName="amac.xlsx" filterable="true" proxyURL="${saveUrl}" />
	<kendo:grid-columns>
		<kendo:grid-column title="Amac" field="amac" />
		<kendo:grid-column title="Aciklama" field="ackiama" hidden="true" />
		<kendo:grid-column title="Status" field="status" hidden="true"/>
		<kendo:grid-column title="Create Date" field="createDate" format="{0: dd-MMM-yyyy}" hidden="true"/>
		<kendo:grid-column title="OrderIndex" field="orderIndex" hidden="true"/>
		<kendo:grid-column title="Department" field="department" editor="deptsEditor" hidden="true" />
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text="" />
					<kendo:grid-column-commandItem name="destroy"  text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
		</sec:authorize>
	</kendo:grid-columns>
	<kendo:dataSource pageSize="10" serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${amacReadUrl}" data="<%=request.getParameter(\"dataDept\")%>" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${amacUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${amacDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${amacCreateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>

		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="amacId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="amac" type="string" />
					<kendo:dataSource-schema-model-field name="ackiama" type="string" />
					<kendo:dataSource-schema-model-field name="status" type="number" />
					<kendo:dataSource-schema-model-field name="createDate" type="date" />
					<kendo:dataSource-schema-model-field name="orderIndex" type="number" />
					<kendo:dataSource-schema-model-field name="department" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>

<kendo:grid-detailTemplate id="template_amac">
	<jsp:include page="hedef.jsp">
		<jsp:param value="#=amacId#" name="amacId" />
		<jsp:param value="template_hedef" name="temp_hedef" />
	</jsp:include>
</kendo:grid-detailTemplate>


<kendo:grid-detailTemplate id="template_hedef">
	<kendo:tabStrip name="tabStrip_#=hedefId#">
		<kendo:tabStrip-animation>
			<kendo:tabStrip-animation-open effects="fadeIn"></kendo:tabStrip-animation-open>
		</kendo:tabStrip-animation>
		<kendo:tabStrip-items>

			<kendo:tabStrip-item text="Performans Hedefi" selected="true">
				<kendo:tabStrip-item-content>
					<jsp:include page="performansHedef.jsp">
						<jsp:param value="#=hedefId#" name="hedef2" />
						<jsp:param value="template_performansHedef" name="temp_perf" />
					</jsp:include>
				</kendo:tabStrip-item-content>
			</kendo:tabStrip-item>

			<%-- <kendo:tabStrip-item text="Performans">
				<kendo:tabStrip-item-content>
					<jsp:include page="performans.jsp">
						<jsp:param value="#=hedefId#" name="hedef1" />
					</jsp:include>
				</kendo:tabStrip-item-content>
			</kendo:tabStrip-item> --%>




		</kendo:tabStrip-items>
	</kendo:tabStrip>
</kendo:grid-detailTemplate>


<kendo:grid-detailTemplate id="template_performansHedef">
	<kendo:tabStrip name="tabStrip_hedef_#=perId#">
		<kendo:tabStrip-animation>
			<kendo:tabStrip-animation-open effects="fadeIn"></kendo:tabStrip-animation-open>
		</kendo:tabStrip-animation>
		<kendo:tabStrip-items>

			<kendo:tabStrip-item text="Gostergeler" selected="true">
				<kendo:tabStrip-item-content>
					<jsp:include page="gostergeler.jsp">
						<jsp:param value="#=perId#" name="perf1" />
						<jsp:param value="gostegreler_temp" name="goster_temp" />
						<jsp:param value="turDropDownEditor" name="nameTurEditor" />
						<jsp:param value="adetDropDownEditor" name="nameAdetEditor" />
						<jsp:param value="olcumDropDownEditor" name="nameOlcumEditor" />
					</jsp:include>
				</kendo:tabStrip-item-content>
			</kendo:tabStrip-item>
			
			
			<kendo:tabStrip-item text="Faaliyet">
				<kendo:tabStrip-item-content>
					<jsp:include page="faaliyet.jsp">
						<jsp:param value="#=perId#" name="perf2" />
						<jsp:param value="faaliyet_temp" name="faaliyet_temp" />
					</jsp:include>
				</kendo:tabStrip-item-content>
			</kendo:tabStrip-item>
			
		</kendo:tabStrip-items>
	</kendo:tabStrip>
</kendo:grid-detailTemplate>

<kendo:grid-detailTemplate id="gostegreler_temp">
	<jsp:include page="gerceklesen.jsp">
		<jsp:param value="#=gostergeId#" name="gostergeId" />
		<jsp:param value="quarter1editorUser" name="editorquart1User" />
		<jsp:param value="quarter2editorUser" name="editorquart2User" />
		<jsp:param value="quarter3editorUser" name="editorquart3User" />
		<jsp:param value="quarter4editorUser" name="editorquart4User" />
		<jsp:param value="quarter1editorAdmin" name="editorquart1Admin" />
		<jsp:param value="quarter2editorAdmin" name="editorquart2Admin" />
		<jsp:param value="quarter3editorAdmin" name="editorquart3Admin" />
		<jsp:param value="quarter4editorAdmin" name="editorquart4Admin" />
	</jsp:include>
</kendo:grid-detailTemplate>

<kendo:grid-detailTemplate id="faaliyet_temp">
	<jsp:include page="budget.jsp">
		<jsp:param value="#=faaliyetId#" name="faaliyetId" />
	</jsp:include>
</kendo:grid-detailTemplate>

<script>
	function deptsEditor(container, options) {
		var deptDS = new kendo.data.DataSource({
			transport : {
				read : {
					url : "${pageContext.request.contextPath}/dept/readall",
					type : "GET"
				}
			},
			schema : {
				model : {
					id : "deptId",
					fields : {
						name : {
							type : "string"
						}
					}
				}
			}
		});

		$("<select multiple='multiple' data-bind='value :"+options.field+"'/>")
				.appendTo(container).kendoMultiSelect({
					dataTextField : "name",
					dataValueField : "deptId",
					dataSource : deptDS,
					value : options.field
				});
	}

	function turDropDownEditor(container, options) {
		$(
				'<input data-text-field="adi" data-value-field="turId" data-bind="value:' + options.field + '"/>')
				.appendTo(container)
				.kendoDropDownList(
						{
							autoBind : false,
							dataSource : {
								transport : {
									read : "${pageContext.request.contextPath}/tur/read",
									type : "GET"
								}
							}
						});
	};

	function adetDropDownEditor(container, options) {
		$(
				'<input data-text-field="adi" data-value-field="adetId" data-bind="value:' + options.field + '"/>')
				.appendTo(container)
				.kendoDropDownList(
						{
							autoBind : false,
							dataSource : {
								transport : {
									read : "${pageContext.request.contextPath}/adet/read",
									type : "GET"
								}
							}
						});
	};

	function olcumDropDownEditor(container, options) {
		$(
				'<input data-text-field="olcumSekil" data-value-field="olcumId" data-bind="value:' + options.field + '"/>')
				.appendTo(container)
				.kendoDropDownList(
						{
							autoBind : false,
							dataSource : {
								transport : {
									read : "${pageContext.request.contextPath}/olcum/read",
									type : "GET"
								}
							}
						});
	};
	
	

	function quarter1editorUser(container, options) {
		$('<input name="quarter1.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter1.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

	};

	function quarter2editorUser(container, options) {
		$('<input name="quarter2.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter2.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

	};

	function quarter3editorUser(container, options) {
		$('<input name="quarter3.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter3.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

	};

	function quarter4editorUser(container, options) {
		$('<input name="quarter4.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter4.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

	};

	function quarter1editorAdmin(container, options) {
		$('<input name="quarter1.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter1.targetValue" placeholder="Target"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();

	};

	function quarter2editorAdmin(container, options) {
		$('<input name="quarter2.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter2.targetValue" placeholder="Target"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	};

	function quarter3editorAdmin(container, options) {
		$('<input name="quarter3.realValue"  placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter3.targetValue" placeholder="Target"   style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	};

	function quarter4editorAdmin(container, options) {
		$('<input name="quarter4.realValue" placeholder="Real"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();
		$('<input name="quarter4.targetValue" placeholder="Target"   style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	};


	$("#grid .k-grid-content").css({
	    "overflow-y": "hidden"
	});
	
</script>



