<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:url value="/maliyeti/read" var="maliyetiReadUrl" />
<c:url value="/maliyeti/update" var="maliyetiUpdateUrl" />
<c:url value="/maliyeti/delete" var="maliyetiDeleteUrl" />
<c:url value="/maliyeti/create" var="maliyetiCreateUrl" />


<kendo:grid name="grid_mali" sortable="true" filterable="true" selectable="true">
	<kendo:grid-editable mode="popup" />
	<kendo:grid-toolbar>
		<kendo:grid-toolbarItem name="create" />
	</kendo:grid-toolbar>
	<kendo:grid-columns>
		<kendo:grid-column title="Yil" field="yil" />
		<kendo:grid-column title="Ek1" field="ek1" />
		<kendo:grid-column title="Ek2" field="ek2" />
		<kendo:grid-column title="Ek3" field="ek3" />
		<kendo:grid-column title="Ek4" field="ek4" />
		<kendo:grid-column title="Ek5" field="ek5" />
		<kendo:grid-column title="Ek6" field="ek6" />
		<kendo:grid-column title="Ek7" field="ek7" />
		<kendo:grid-column title="Ek8" field="ek8" />
		<kendo:grid-column title="Ek9" field="ek9" />
		<kendo:grid-column title="T1" field="t1" />
		<kendo:grid-column title="T2" field="t2" />
		<kendo:grid-column title="T3" field="t3" />
		<kendo:grid-column title="T4" field="t4" />
		<kendo:grid-column title="&nbsp;" width="180px">
			<kendo:grid-column-command>
				<kendo:grid-column-commandItem name="edit" />
				<kendo:grid-column-commandItem name="destroy" />
			</kendo:grid-column-command>
		</kendo:grid-column>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read  url="${maliyetiReadUrl}" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${maliyetiUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${maliyetiDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${maliyetiCreateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>


		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="malId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="yil" type="string" />
					<kendo:dataSource-schema-model-field name="ek1" type="number" />
					<kendo:dataSource-schema-model-field name="ek2" type="number" />
					<kendo:dataSource-schema-model-field name="ek3" type="number" />
					<kendo:dataSource-schema-model-field name="ek4" type="number" />
					<kendo:dataSource-schema-model-field name="ek5" type="number" />
					<kendo:dataSource-schema-model-field name="ek6" type="number" />
					<kendo:dataSource-schema-model-field name="ek7" type="number" />
					<kendo:dataSource-schema-model-field name="ek8" type="number" />
					<kendo:dataSource-schema-model-field name="ek9" type="number" />
					<kendo:dataSource-schema-model-field name="t1" type="number" />
					<kendo:dataSource-schema-model-field name="t2" type="number" />
					<kendo:dataSource-schema-model-field name="t3" type="number" />
					<kendo:dataSource-schema-model-field name="t4" type="number" />					
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>
