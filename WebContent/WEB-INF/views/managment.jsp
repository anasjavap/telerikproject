<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="temp" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<c:url value="/user/read" var="userReadUrl" />
<c:url value="/user/update" var="userUpdateUrl" />
<c:url value="/user/delete" var="userDeleteUrl" />
<c:url value="/user/create" var="userCreateUrl" />

<c:url value="/role/read" var="roleUrl" />

<c:url value="/dept/read" var="deptReadUrl" />
<c:url value="/dept/update" var="deptUpdateUrl" />
<c:url value="/dept/delete" var="deptDeleteUrl" />
<c:url value="/dept/create" var="deptCreateUrl" />


<temp:header />

<div style="float: left; width: 20%; padding: 10px 10px;background-color: white;">
	<kendo:grid name="grid_dept" sortable="true" filterable="true" selectable="true" change="onChange">
		<sec:authorize access="hasRole('ROLE_ADMIN')">
		<kendo:grid-editable mode="popup" />		
		<kendo:grid-toolbar >
			<kendo:grid-toolbarItem name="create" text=""/>
			<kendo:grid-toolbarItem name="excel" text=""/>
		</kendo:grid-toolbar>
		</sec:authorize>
		<kendo:grid-excel fileName="dept.xlsx" filterable="true" proxyURL="${saveUrl}" />
		<kendo:grid-columns>
			<kendo:grid-column title="Name" field="name" />		
			<sec:authorize access="hasRole('ROLE_ADMIN')">	
			<kendo:grid-column title="&nbsp;" width="75px" >
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text=""/>
					<kendo:grid-column-commandItem name="destroy" text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
			</sec:authorize>
		</kendo:grid-columns>
		<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
			<kendo:dataSource-transport>
				<kendo:dataSource-transport-read url="${deptReadUrl}" type="POST" dataType="json" contentType="application/json" />
				<kendo:dataSource-transport-update url="${deptUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-destroy url="${deptDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-create url="${deptCreateUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-parameterMap>
					<script>
						function parameterMap(options) {
							return JSON.stringify(options);
						}
					</script>
				</kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport>
			<kendo:dataSource-schema>
				<kendo:dataSource-schema-model id="deptId">
					<kendo:dataSource-schema-model-fields>
						<kendo:dataSource-schema-model-field name="name" type="string" />
					</kendo:dataSource-schema-model-fields>
				</kendo:dataSource-schema-model>
			</kendo:dataSource-schema>
		</kendo:dataSource>
	</kendo:grid>
	
</div>

<div class="layoutpagecontent">


	<kendo:grid name="grid" pageable="true" sortable="true" filterable="true">

		    <kendo:grid-editable mode="popup" />
		    <kendo:grid-toolbar>
			<kendo:grid-toolbarItem name="create" text=""/>
		    </kendo:grid-toolbar>
		    <kendo:grid-columns>
			<kendo:grid-column title="Adı" field="firstName" />
			<kendo:grid-column title="Soyadı" field="lastName" />
			<kendo:grid-column title="Kullanıcı Adı" field="username" />
			<kendo:grid-column title="Şifre" field="password" editor="passwordEditor" template="#:password == null ? ' ' : '●'.repeat(password.length) #" />
			<kendo:grid-column title="Aktif" field="active" />
			<kendo:grid-column title="Adres" field="address" />
			<kendo:grid-column title="Telefon" field="phone" />
			<kendo:grid-column title="Yetki" field="role" editor="roleDropDownEditor" template="#=role.name#" />
			<kendo:grid-column title="&nbsp;" width="75px">
				<kendo:grid-column-command>
					<kendo:grid-column-commandItem name="edit" text="" />
					<kendo:grid-column-commandItem name="destroy" text=""/>
				</kendo:grid-column-command>
			</kendo:grid-column>
		</kendo:grid-columns>
		<kendo:dataSource pageSize="10"   serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
			<kendo:dataSource-transport>
				<kendo:dataSource-transport-read url="${userReadUrl}" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-update url="${userUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-destroy url="${userDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-create url="${userCreateUrl}" dataType="json" type="POST" contentType="application/json" />

				<kendo:dataSource-transport-parameterMap>
					<script>
						function parameterMap(options) {
							return JSON.stringify(options);
						}
					</script>
				</kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport>


			<kendo:dataSource-schema >
				<kendo:dataSource-schema-model id="id">
					<kendo:dataSource-schema-model-fields>
						<kendo:dataSource-schema-model-field name="firstName" type="string" />
						<kendo:dataSource-schema-model-field name="lastName" type="string" />
						<kendo:dataSource-schema-model-field name="username" type="string" />
						<kendo:dataSource-schema-model-field name="password" type="string" />
						<kendo:dataSource-schema-model-field name="active" type="boolean" />
						<kendo:dataSource-schema-model-field name="address" type="string" />
						<kendo:dataSource-schema-model-field name="phone" type="string" />
						<kendo:dataSource-schema-model-field name="role" defaultValue="user"/>
					</kendo:dataSource-schema-model-fields>
				</kendo:dataSource-schema-model>
			</kendo:dataSource-schema>
		</kendo:dataSource>
	</kendo:grid>
</div>

<script type="text/javascript">

	function passwordEditor(container, options) {
		$(
				'<input type="password" required data-bind="value:' + options.field + '"/>')
				.appendTo(container);
	};

	function roleDropDownEditor(container, options) {
		$(
				'<input data-text-field="name" data-value-field="id" data-bind="value:' + options.field + '"/>')
				.appendTo(container).kendoDropDownList({
					autoBind : false,
					dataSource : {
						transport : {
							read : "${roleUrl}"
						}
					}
				});
	}

	function onChange(e) {
		grid = e.sender;
		var currentDataItem = grid.dataItem(this.select());
		var grid = $("#grid").data("kendoGrid");
		grid.dataSource.options.transport.read.url = "${pageContext.request.contextPath}/user/read?id="
				+ currentDataItem.id;
		grid.dataSource.read();
		grid.refresh();
	}
	
</script>

<temp:footer/>