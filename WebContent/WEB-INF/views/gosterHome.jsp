<%@page import="com.telerik.datamodel.StrategyPlanAmaclar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="temp" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<c:url value="/dept/read" var="deptReadUrl" />

<c:url value="/gosterge/readForHome?id=" var="gostergeReadUrl" />
<c:url value="/gosterge/update" var="gostergeUpdateUrl" />
<c:url value="/gosterge/delete" var="gostergeDeleteUrl" />

<temp:header />

<div style="float: left; width: 20%; padding: 10px 10px; background-color: white;">

	<kendo:grid name="grid_dept" sortable="true" filterable="true" selectable="true" change="onChange_gos">
		<kendo:grid-columns>
			<kendo:grid-column title="Name" field="name" />
		</kendo:grid-columns>

		<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
			<kendo:dataSource-transport>
				<kendo:dataSource-transport-read url="${deptReadUrl}" type="POST" dataType="json" contentType="application/json" />
				<kendo:dataSource-transport-parameterMap>
					<script>
						function parameterMap(options) {
							return JSON.stringify(options);
						}
					</script>
				</kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport>
			<kendo:dataSource-schema>
				<kendo:dataSource-schema-model id="deptId">
					<kendo:dataSource-schema-model-fields>
						<kendo:dataSource-schema-model-field name="name" type="string" />
					</kendo:dataSource-schema-model-fields>
				</kendo:dataSource-schema-model>
			</kendo:dataSource-schema>
		</kendo:dataSource>
	</kendo:grid>

</div>

<div class="layoutpagecontent">

	<kendo:grid name="grid_gos" pageable="true" sortable="true" scrollable="false" detailTemplate="gostegreler_temp">
		<kendo:grid-editable mode="popup" />
	    <kendo:grid-filterable mode="row"/>
	    
		<kendo:grid-columns>
			<kendo:grid-column title="Adi" field="adi">
				<kendo:grid-column-filterable>
					<kendo:grid-column-filterable-cell operator="contains" />
				</kendo:grid-column-filterable>
			</kendo:grid-column>
			<kendo:grid-column title="Aciklama" field="ackiama" filterable="false"/>
			<kendo:grid-column title="Status" field="status" hidden="true"  filterable="false"/>
			<kendo:grid-column title="Create Date" field="createDate" format="{0: dd-MMM-yyyy}" hidden="true"  filterable="false"/>
			<kendo:grid-column title="OrderIndex" field="orderIndex" hidden="true"  filterable="false"/>
			<kendo:grid-column title="Tur" field="gostergelerTur" editor="turDropDownEditor" template="#=gostergelerTur.adi#" filterable="false" />
			<kendo:grid-column title="Oclum" field="oclumsiklik" editor="olcumDropDownEditor" template="#=oclumsiklik.olcumSekil#" filterable="false" />
			<kendo:grid-column title="Adet" field="adetcCinsi" editor="adetDropDownEditor" template="#=adetcCinsi.adi#" filterable="false"/>


			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<kendo:grid-column title="&nbsp;" width="75px">
					<kendo:grid-column-command>
						<kendo:grid-column-commandItem name="edit" text="" />
						<kendo:grid-column-commandItem name="destroy" text="" />
					</kendo:grid-column-command>
				</kendo:grid-column>
			</sec:authorize>

		</kendo:grid-columns>
		<kendo:dataSource pageSize="10" serverPaging="true" serverSorting="true">
			<kendo:dataSource-transport>
				<kendo:dataSource-transport-read url="${gostergeReadUrl}" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-update url="${gostergeUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-destroy url="${gostergeDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
				<kendo:dataSource-transport-parameterMap>
					<script>
						function parameterMap(options) {
							return JSON.stringify(options);
						}
					</script>
				</kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport>

			<kendo:dataSource-schema>
				<kendo:dataSource-schema-model id="gostergeId">
					<kendo:dataSource-schema-model-fields>
						<kendo:dataSource-schema-model-field name="adi" type="string" />
						<kendo:dataSource-schema-model-field name="ackiama" type="string" />
						<kendo:dataSource-schema-model-field name="status" type="number" />
						<kendo:dataSource-schema-model-field name="createDate" type="date" />
						<kendo:dataSource-schema-model-field name="orderIndex" type="number" />
						<kendo:dataSource-schema-model-field name="gostergelerTur" />
						<kendo:dataSource-schema-model-field name="oclumsiklik" />
						<kendo:dataSource-schema-model-field name="adetcCinsi" />
					</kendo:dataSource-schema-model-fields>
				</kendo:dataSource-schema-model>
			</kendo:dataSource-schema>
		</kendo:dataSource>
	</kendo:grid>
</div>

<kendo:grid-detailTemplate id="gostegreler_temp">
	<jsp:include page="gerceklesen.jsp">
		<jsp:param value="#=gostergeId#" name="gostergeId" />
		<jsp:param value="quarter1editorUser" name="editorquart1User" />
		<jsp:param value="quarter2editorUser" name="editorquart2User" />
		<jsp:param value="quarter3editorUser" name="editorquart3User" />
		<jsp:param value="quarter4editorUser" name="editorquart4User" />
		<jsp:param value="quarter1editorAdmin" name="editorquart1Admin" />
		<jsp:param value="quarter2editorAdmin" name="editorquart2Admin" />
		<jsp:param value="quarter3editorAdmin" name="editorquart3Admin" />
		<jsp:param value="quarter4editorAdmin" name="editorquart4Admin" />
	</jsp:include>
</kendo:grid-detailTemplate>

<script type="text/javascript">
function onChange_gos(e) {
	grid = e.sender;
	var currentDataItem = grid.dataItem(this.select());
	var grid = $("#grid_gos").data("kendoGrid");
	grid.dataSource.options.transport.read.url = "${pageContext.request.contextPath}/gosterge/readForHome?id="
			+ currentDataItem.id;
	grid.dataSource.read();
	grid.refresh();
}


function turDropDownEditor(container, options) {
	$(
			'<input data-text-field="adi" data-value-field="turId" data-bind="value:' + options.field + '"/>')
			.appendTo(container)
			.kendoDropDownList(
					{
						autoBind : false,
						dataSource : {
							transport : {
								read : "${pageContext.request.contextPath}/tur/read",
								type : "GET"
							}
						}
					});
};

function adetDropDownEditor(container, options) {
	$(
			'<input data-text-field="adi" data-value-field="adetId" data-bind="value:' + options.field + '"/>')
			.appendTo(container)
			.kendoDropDownList(
					{
						autoBind : false,
						dataSource : {
							transport : {
								read : "${pageContext.request.contextPath}/adet/read",
								type : "GET"
							}
						}
					});
};

function olcumDropDownEditor(container, options) {
	$(
			'<input data-text-field="olcumSekil" data-value-field="olcumId" data-bind="value:' + options.field + '"/>')
			.appendTo(container)
			.kendoDropDownList(
					{
						autoBind : false,
						dataSource : {
							transport : {
								read : "${pageContext.request.contextPath}/olcum/read",
								type : "GET"
							}
						}
					});
};


function quarter1editorUser(container, options) {
	$('<input name="quarter1.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter1.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

};

function quarter2editorUser(container, options) {
	$('<input name="quarter2.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter2.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

};

function quarter3editorUser(container, options) {
	$('<input name="quarter3.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter3.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

};

function quarter4editorUser(container, options) {
	$('<input name="quarter4.realValue" placeholder="Real" style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter4.targetValue" placeholder="Target" disabled="disabled" style="width:70px"/>').appendTo(container).kendoNumericTextBox();

};

function quarter1editorAdmin(container, options) {
	$('<input name="quarter1.realValue" placeholder="Real"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter1.targetValue" placeholder="Target"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();

};

function quarter2editorAdmin(container, options) {
	$('<input name="quarter2.realValue" placeholder="Real"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter2.targetValue" placeholder="Target"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();
};

function quarter3editorAdmin(container, options) {
	$('<input name="quarter3.realValue"  placeholder="Real"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter3.targetValue" placeholder="Target"   style="width:70px"/>').appendTo(container).kendoNumericTextBox();
};

function quarter4editorAdmin(container, options) {
	$('<input name="quarter4.realValue" placeholder="Real"  style="width:70px"/>').appendTo(container).kendoNumericTextBox();
	$('<input name="quarter4.targetValue" placeholder="Target"   style="width:70px"/>').appendTo(container).kendoNumericTextBox();
};



</script>


<temp:footer />