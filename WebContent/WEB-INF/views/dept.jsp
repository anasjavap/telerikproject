<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>

<%
	HashMap<String, Object> dataDept = new HashMap<String, Object>();
	dataDept.put("deptId", "#=deptId#");
%>

<c:url value="/dept/read" var="deptReadUrl" />
<c:url value="/dept/update" var="deptUpdateUrl" />
<c:url value="/dept/delete" var="deptDeleteUrl" />
<c:url value="/dept/create" var="deptCreateUrl" />



<kendo:grid name="grid_dept" sortable="true" filterable="true" selectable="true" change="onChange_dept()" dataBound="test">
	<kendo:grid-editable mode="popup" />
	<kendo:grid-toolbar>
		<kendo:grid-toolbarItem name="create" />
		<kendo:grid-toolbarItem name="excel" />
	</kendo:grid-toolbar>
	<kendo:grid-excel fileName="dept.xlsx" filterable="true" proxyURL="${saveUrl}" />

	<kendo:grid-columns>
		<kendo:grid-column title="Name" field="name" />
		<kendo:grid-column title="&nbsp;" width="75px" >
			<kendo:grid-column-command>
				<kendo:grid-column-commandItem name="edit" text="" />
				<kendo:grid-column-commandItem name="destroy" text=""/>
			</kendo:grid-column-command>
		</kendo:grid-column>
	</kendo:grid-columns>
	<kendo:dataSource serverPaging="true" serverSorting="true" serverFiltering="true" serverGrouping="true">
		<kendo:dataSource-transport>
			<kendo:dataSource-transport-read url="${deptReadUrl}" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-update url="${deptUpdateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-destroy url="${deptDeleteUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-create url="${deptCreateUrl}" dataType="json" type="POST" contentType="application/json" />
			<kendo:dataSource-transport-parameterMap>
				<script>
					function parameterMap(options) {
						return JSON.stringify(options);
					}
				</script>
			</kendo:dataSource-transport-parameterMap>
		</kendo:dataSource-transport>


		<kendo:dataSource-schema>
			<kendo:dataSource-schema-model id="deptId">
				<kendo:dataSource-schema-model-fields>
					<kendo:dataSource-schema-model-field name="name" type="string" />
				</kendo:dataSource-schema-model-fields>
			</kendo:dataSource-schema-model>
		</kendo:dataSource-schema>
	</kendo:dataSource>
</kendo:grid>

